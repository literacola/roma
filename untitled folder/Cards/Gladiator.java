package Cards;

import java.util.ArrayList;

import Game.Board;
import Game.Disk;
import Game.Player;

public class Gladiator extends Card{

	private Board board;

	public Gladiator(Board board) {
		//name, description, cost, defense
		super("Gladiator", "opponents face up card returned to opponent",6,5);
		this.board = board;
	}

	public void activate(){

		Disk[] diceDisk = board.getDiceDisks();
		ArrayList<Disk> ocDisks;
		int[] pos = super.getPosition(this, diceDisk);

		//System.out.println("I am sitting on player " + pos[1] + "'s side and sitting on dice disk number " + pos[0]);

		if(pos[1] == 0) {  //please god change this
			ocDisks=board.getOccupiedDisks(1);

		} else {
			ocDisks=board.getOccupiedDisks(0);
		}

		
		System.out.println("You just played a Gladiator! choose opponent's card!");
		

	}

}
