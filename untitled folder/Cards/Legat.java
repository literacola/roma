package Cards;
import Game.Board;
import Game.Config;
import Game.Disk;
import Game.Player;





public class Legat extends Card {
	
	Board board;
	
	public Legat(Board board) {
		//name, description, cost, defense
		super("Legat", "Gets 1 vp from the stockpile for every unoccupied dice disk",5,2);
		this.board = board;
	}
	
	public void activate(){
		
		Disk[] diceDisk = board.getDiceDisks();
		int numSes;
		int[] pos = super.getPosition(this, diceDisk);
		
		//System.out.println("I am sitting on player " + pos[1] + "'s side and sitting on dice disk number " + pos[0]);
		
		if(pos[1] == 0) {  //please god change this
			numSes=board.getNumUnoccupiedDisks(1);
			
		} else {
			numSes=board.getNumUnoccupiedDisks(0);
		}
		
		Player.getCurrentPlayer().addSestertii(numSes);
		System.out.println("You just played a Legat! You got an extra " + numSes + " sestertii!");
		
	}
	
}



//
//
//public class Legat implements CardInt  {
//	
//	private String name;
//	private String description;
//	private int cost;
//	private int defense;
//	private Board board;
//	
//	public Legat(Board board) {
//		
//		this.name = "Legat";
//		this.description = "Gets 1 vp from the stockpile for every unoccupied dice disk";
//		this.cost=5;
//		this.defense=2;
//		this.board = board;
//	}
//	
//	
//	public void activate() {
//		
//		
//		
//	}
//	
//	
//}