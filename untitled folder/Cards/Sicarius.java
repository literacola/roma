package Cards;
import Game.*;


public class Sicarius extends Card {

	private Board board;
	
	public Sicarius(Board board) {
		//name, description, cost, defense
		super("Sicarius", "Discards opposing character card and this card",9,2);
		this.board = board;
	}
	
	
	public void activate() {
		
		Disk[] diceDisk = board.getDiceDisks();
		int pos[] = super.getPosition(this, diceDisk);
		//pos[0] = dice disk
		//pos[1] = player
		int diskPos = pos[Config.DICE_DISK_POS];
		int playerPos = pos[Config.PLAYER_POS];
		
		diceDisk[diskPos].removeCard(playerPos);
		
		if (Player.getCurrentPlayer().getPlayerNum() == 0) {
			diceDisk[diskPos].removeCard(1);
		} else {
			diceDisk[diskPos].removeCard(0);
		}
		System.out.println("You just played a Sicarius");
		
	}

		
}
