package Cards;

import Game.Board;
import Game.Config;
import Game.Disk;


//should this be abstract?
public class Card {
	private String name;
	private String description;
	private int cost;
	private int defense;
	
	
	public Card(String name, String description, int cost, int defense) {
		this.name = name;
		this.description = description;
		this.cost=cost;
		this.defense=defense;
		
	}
	
	public int getDefense() {
		return defense;
	}
	
	public int getCost() {
		return cost;
	}
	
	public String getDescription() {
		return description;
	}
	
	public String getName() {
		return name;
	}
	
	public void decrementDefense(int num) {
		defense-=num;
		assert(defense >= 0);
	}
	
	public void activate() {
		
	}

	public int[] getPosition(Card card, Disk[] disks) {
		int[] pos = new int[2];

		for(int i=0; i<disks.length; i++ ) {
			for(int j=0; j<Config.NUM_PLAYERS; j++) {
				if(disks[i].getCard(j) != null) {
					if(disks[i].getCard(j).equals(card)) {
						pos[0] = i; //which dice disk
						pos[1] = j; //which player
					}
				}
			}
		}		
		return pos;	
	}
	
}
