package Cards;

import java.io.IOException;
import java.util.ArrayList;

import org.w3c.dom.UserDataHandler;

import Game.Board;
import Game.Die;
import Game.Disk;
import Game.Player;
import Game.UserInput;

public class Consul extends Card{

	Board board;

	public Consul(Board board) {
		//name, description, cost, defense
		super("Consul", "increase/decrease action dice value by 1",3,3);
		this.board = board;
	}

	public void activate() throws IOException{

		Disk[] diceDisk = board.getDiceDisks();
		int[] pos = super.getPosition(this, diceDisk);
		int diceChoice;
		int incOrDec;
		
		//this shouldn't rely on currentPlayer -- this will cause issues with the duplicator card
		
		ArrayList<Die> unusedDice = Player.getCurrentPlayer().getUnusedDice();
		
		
		for(int i = 0; i<unusedDice.size(); i++){
				System.out.println("dice " + i + ": " + unusedDice.get(i).getDieValue());
		}
			
			
		diceChoice = UserInput.intUserInput("Which die would you like to use?",0,unusedDice.size());
		incOrDec = UserInput.intUserInput("Do you want to increase or decrease - 1 for increase, 0 for decrease?",0,1);
		
		if(incOrDec == 1){
			Player.getCurrentPlayer().incrementDie(diceChoice);
		} else {
			Player.getCurrentPlayer().decrementDie(diceChoice);
		}
		
		
	}
}
