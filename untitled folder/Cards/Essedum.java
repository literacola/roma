package Cards;

import Game.Board;
import Game.Config;
import Game.Disk;
import Game.Player;

public class Essedum extends Card {

	Board board;

	public Essedum(Board board) {
		//name, description, cost, defense
		super("Essedum", "Reduce opponents face up card defence by 2",6,3);
		this.board = board;
	}

	public void activate(){

		Disk[] diceDisk = board.getDiceDisks();
		int[] pos = super.getPosition(this, diceDisk);

		
		int diskPos = pos[Config.DICE_DISK_POS];

		int opponent = Player.getOpponent();

		System.out.println("you've played sicarius" );
		
		for(int i = 0; i<Config.NUM_DICEDISKS; i++){

			if (diceDisk[i].getCard(opponent) != null){
				diceDisk[i].getCard(opponent).decrementDefense(2);
				System.out.println(diceDisk[diskPos].getCard(opponent).getName() + " defense is now " + diceDisk[diskPos].getCard(opponent).getDefense() );
			}
		}
		
		

	}
}
