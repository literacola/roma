package Game;

import java.io.*;
import java.util.Scanner;



///argh this whole thing should probably be static
public class UserInput {

	static boolean mockInput;
	static String currentLine;
	static BufferedReader br;
	static Scanner input; 
	private static Roma roma;
	public UserInput() {
		mockInput=false;
	}
	
	
	public UserInput(String file, Roma roma) throws FileNotFoundException {
		System.out.println("Setting roma to take in test input from a textfile");
		mockInput=true;
		br = new BufferedReader(new FileReader(file));
		UserInput.roma = roma;
	}
	
	
	public String getUserInput(String prompt) throws IOException {
		
		String str="";

		if(mockInput) {
			if((currentLine = br.readLine()) != null) {
				str=currentLine;
			} else {
				roma.stopGame();
			}
			
		} else {
				System.out.print(prompt);
				input = new Scanner(System.in);
				if(input.hasNext()){
					str = input.nextLine();
				}
		}		
		return(str);
	}
	
	public static String staticUserInput(String prompt) throws IOException {
		
		String str="";

		if(mockInput) {
			if((currentLine = br.readLine()) != null) {
				str=currentLine;
			} else {
				roma.stopGame();
			}
			
		} else {
				System.out.print(prompt);
				input = new Scanner(System.in);
				if(input.hasNext()){
					str = input.nextLine();
				}
		}		
		return(str);
	}
	
	public static int intUserInput(String prompt) throws IOException {
		
		String tmp="";
		int ret=-1;

		if(mockInput) {
			if((currentLine = br.readLine()) != null) {
				tmp=currentLine;
				ret=Integer.parseInt(tmp);
			} else {
				roma.stopGame();
			}
		} else {
			
			System.out.print(prompt);
			input = new Scanner(System.in);
			if(input.hasNextInt()){ 
				ret = input.nextInt();
			}
		}		
		
	return(ret);
}
		
	
	
	public static int intUserInput(String prompt, int lower, int upper) throws IOException {
		
		String tmp="";
		int ret=-1;

		if(mockInput) {
			if((currentLine = br.readLine()) != null) {
				tmp=currentLine;
				ret=Integer.parseInt(tmp);
			} else {
				roma.stopGame();
			}
		} else {
			
			System.out.print(prompt);
			input = new Scanner(System.in);
			if(input.hasNextInt()){ 
				ret = input.nextInt();
			}
			
			while(ret < lower || ret > upper) {
				System.out.print(prompt);
				input = new Scanner(System.in);
				if(input.hasNextInt()){ 
					ret = input.nextInt();
				}	
			}
			
		}		
		
	return(ret);
}
	
	
	
	
}
