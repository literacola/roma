package Game;

import java.io.IOException;
import java.util.*;

import Cards.*;

public class Board{


	private Stack<Card> discardPile = new Stack<Card>();
	private static Disk[] diceDisks = new Disk[Config.NUM_DICEDISKS]; 
	private int VPStockPile;
	private List<Card> deck = new ArrayList<Card>();



	public Board() {
		createCards();
		createDisks();
		//this.VPStockPile = 36;
		this.VPStockPile = Config.GAME_VICTORY_POINTS;
	}

	//overload this so numCards doesn't need to be specified?
	public void swapCards(Player A, Player B, int numCards) throws IOException {
		ArrayList<Card> playerAEscrow = new ArrayList<Card>();
		ArrayList<Card> playerBEscrow = new ArrayList<Card>();
		String choice;

		System.out.println(A.getUserName() + ", choose which cards you would like to swap: ");
		for(int i=0; i<numCards; i++) {
			for(int j=0; j<A.getAllCardsInHand().size(); j++) {
				System.out.println("[" + j + "] " + A.getCard(j).getName()); 
			}

			choice = UserInput.staticUserInput("Enter card to swap: ");
			playerAEscrow.add(A.getCard(Integer.parseInt(choice)));
			A.removeCard(Integer.parseInt(choice));	
		}

		System.out.println(B.getUserName() + ", choose which cards you would like to swap: ");
		for(int i=0; i<numCards; i++) {
			for(int j=0; j<B.getAllCardsInHand().size(); j++) {
				System.out.println("[" + j + "] " + B.getCard(j).getName()); 
			}

			choice = UserInput.staticUserInput("Enter card to swap: ");
			playerBEscrow.add(B.getCard(Integer.parseInt(choice)));
			B.removeCard(Integer.parseInt(choice));	
		}

		A.receiveCards(playerBEscrow);
		B.receiveCards(playerAEscrow);

		System.out.println("\n\n\n");	
	}

	public void playCard(int playerID, Card card, int diskID) {
		diceDisks[diskID].playCard(card, playerID);
		Player.getPlayer(playerID).removeCard(card);  //should I actually be removing the cards?
	}


	public void printBoard(boolean hideCards) {
		String occupiedFlag;
		System.out.println("\n\n\n");
		Card[] cardsOnDisk;

		//print Player 1 cards
		System.out.print("Player 1:               "); //magic
		for(Disk d : diceDisks) {
			cardsOnDisk=d.getCardsOnDisk();

			if(cardsOnDisk[0] != null) { //this is not great
				if(hideCards == true) {
					System.out.print("X                     ");
				} else {
					System.out.print(cardsOnDisk[0].getName()+"          "); //magic numbers
				}
			} else {
				System.out.print("                     ");
			}
		}		


		System.out.println("");
		System.out.print("                  ");
		//print DiceDisks
		for(Disk d : diceDisks) {
			System.out.print("   |     " + (d.getDiskID()+1) + "     |   ");
		}

		System.out.println("");
		//print Player 1 cards
		System.out.print("Player 2:               "); //magic
		for(Disk d : diceDisks) {
			cardsOnDisk=d.getCardsOnDisk();

			if(cardsOnDisk[1] != null) { //this is not great
				if(hideCards == true) {
					System.out.print("X                     ");
				} else {
					System.out.print(cardsOnDisk[1].getName()+"          "); //magic numbers
				}
			} else {
				System.out.print("                     ");
			}
		}


		System.out.println("\n\n\n");
	}


	public void discard(Card card) {
		discardPile.push(card);
	}



	public int getVP(int numVP) {
		int returnedVP=0;

		if(numVP > VPStockPile) {
			returnedVP=numVP - VPStockPile;
			this.VPStockPile = 0;
		} else {
			returnedVP = numVP;
			this.VPStockPile-=returnedVP;
		}

		return(returnedVP);
	}




	public int getNumVPLeft() {
		return(this.VPStockPile);
	}



	//how can I make this suck less?
	private void createCards() {


		//		for(int i= 0; i<Config.NUM_TRIBUNUS; i++) {
		//			TribunusPlebis C = new TribunusPlebis(this);	
		//			drawPile.push(C);
		//		}
		//		
		//		for(int i= 0; i<Config.NUM_SICARIUS; i++) {
		//			Sicarius C = new Sicarius(this);	
		//			drawPile.push(C);
		//		}
		//
		//		for(int i= 0; i<Config.NUM_LEGAT; i++) {
		//			Legat C = new Legat(this);	
		//			drawPile.push(C);
		//		}


		//just creating some extra cards for testing
//		for(int i= 0; i<10; i++) {
//			Legat C = new Legat(this);	
//			//drawPile.push(C);
//			deck.add(C);
//		}
//		for(int i= 0; i<10; i++) {
//			TribunusPlebis C = new TribunusPlebis(this);	
//			//drawPile.push(C);
//			deck.add(C);
//		}
//
//		for(int i= 0; i<10; i++) {
//			Sicarius C = new Sicarius(this);	
//			//drawPile.push(C);
//			deck.add(C);
//		}

		for(int i= 0; i<100; i++) {
			Essedum C = new Essedum(this);	
			//drawPile.push(C);
			deck.add(C);
		}

		//Collections.shuffle(drawPile);
		//Collections.shuffle(drawPile);
		Collections.shuffle(deck);
		Collections.shuffle(deck);

		//		printStack(drawPile);
	}


	private void createDisks() {

		for(int i=0; i<Config.NUM_DICEDISKS; i++) {
			diceDisks[i] = new Disk(this,i);
		}

	}


	private void printStack(Stack<Card> printMe) {

		System.out.println("Printing stack:");
		System.out.println("------------------------------------------------------------------------");
		for(Card c : printMe) {
			System.out.println(c.getName());
		}
		System.out.println("------------------------------------------------------------------------");

	}


	public ArrayList<Card> drawCards(int num) {
		ArrayList<Card> drawnCards = new ArrayList<Card>();
		int numCardsDrawn=0;

		while(numCardsDrawn != num && !deck.isEmpty()) {
			drawnCards.add(deck.remove(0));
			numCardsDrawn++;
		}

		if(num > deck.size()) {
			System.out.println("Not able to draw all requested cards-- drew " + numCardsDrawn  + " instead");
		}

		return(drawnCards);

	}


	public void layCardsFaceDown(int numCards, int playerID) throws IOException {
		int diskChoice;
		Player player = Player.getPlayer(playerID);

		System.out.println("Player " + (playerID+1) + ", please place your cards face down");


		for(int i=0; i<numCards; i++) {
			printBoard(true);
			System.out.println("Please select a location for card " + (i+1) + ": ");
			diskChoice = UserInput.intUserInput("");
			diskChoice-=1;  //since disks start with 0
			playCard(playerID, player.getCard(0), diskChoice);
		}
	}


	public Disk[] getDiceDisks() {
		return(diceDisks);
	}

	public int getNumUnoccupiedDisks(int playerID) {
		int numUnoccupied=0;

		Disk[] disks = getDiceDisks();

		for(Disk d : disks) {
			numUnoccupied+=d.getNumUnoccupied(playerID);
		}
		return(numUnoccupied);
	}

	public List<Card> getDeck() {
		return deck;
	}
	
	public ArrayList<Disk> getUnoccupiedDisks(int playerID) {
		ArrayList<Disk> unOcDisks = new ArrayList<Disk>();

		for(Disk d: diceDisks) {
			if(!d.isOccupied(playerID)) {
				unOcDisks.add(d);
			}
		}

		return(unOcDisks);

	}

	public static Card[] getPlayerCardsOnDiscs(int playerNum) {
		Card[] playerCardsOnDisks = new Card[Config.NUM_DICEDISKS];

		for(int i = 0; i<Config.NUM_DICEDISKS; i++) {
			playerCardsOnDisks[i] = diceDisks[i].getCard(playerNum);
		}

		return playerCardsOnDisks;

	}
	
	
}
