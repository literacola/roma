package Game;

public class Config {
	
	public static final int NUM_DICEDISKS = 6;
	
	public static final int NUM_FACES_DIE = 6;
	
	//Player info
	public static final int NUM_INIT_CARDS = 4;	
	public static final int NUM_ACTION_DICE = 3;
	public static final int NUM_PLAYERS = 2;
	public static final int PLAYER1 = 0;
	public static final int PLAYER2 = 1;
	
	
	public static final int NUM_CARDS_INIT_SWAP = 2;
	public static final int NUM_FACE_DOWN_CARDS = 4;
	
	
	//Number of Cards
	public static final int NUM_SICARIUS = 1;
	public static final int NUM_LEGAT = 2;
	public static final int NUM_TRIBUNUS = 2;
	
	public static final int DICE_DISK_POS = 0;
	public static final int PLAYER_POS = 1;
	
	public static final int GAME_VICTORY_POINTS = 36;
}
