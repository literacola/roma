package Game;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

import Cards.*;


public class Player {

	private String userName;
	private int sestertii; //Coins
	private int victoryPoints;
	private ArrayList<Die> actionDice = new ArrayList<Die>();
	private static Board board;
	private int playerNum;
	private static ArrayList<Player> players = new ArrayList<Player>();  //My gut tells me this is wrong, should this get moved somewhere else?
	private ArrayList<Card> cardsInHand = new ArrayList<Card>();

	
	
	public Player (String userName, Board board, int playerNum){
		
		//convert the rest of these to use setters
		this.userName = userName;
		this.sestertii = 10;
		this.victoryPoints = 10;
		for(int i=0; i<Config.NUM_ACTION_DICE; i++) {
			actionDice.add(new Die());
		}
		
		Player.board = board;
		setPlayerNum(playerNum);
		cardsInHand.addAll(board.drawCards(4));
//		this.displayCards();
	}
	
	
	public static Player getPlayer(int playerID) {
		assert(playerID < Config.NUM_PLAYERS);
		return(players.get(playerID));
	}
	
	
	public void displayActionDiceValues() {
		int[] values = new int[Config.NUM_ACTION_DICE];
		values=this.getDiceValues();
		System.out.print(this.getUserName() + " rolled: ");
		for(int die : values) {
			System.out.print(die + ", ");
		}
		System.out.println("");
		
	}
	
	
	public void receiveCards(ArrayList<Card> cards) {
		this.cardsInHand.addAll(cards);
	}
	
	public void receiveCard(Card card) {
		this.cardsInHand.add(card);
	}
	
	public ArrayList<Card> getAllCardsInHand() {
		return(this.cardsInHand);
	}
	
	public Card getCard(int index) {
		return(this.cardsInHand.get(index));
	}
	
	public Card playCard(int index) {
		Card tmp = this.getCard(index);
		this.removeCard(tmp);
		return(tmp);
	}
	
	public void removeCard(int cardIndex) {
		cardsInHand.remove(cardIndex);
	}
	
	
	public void removeCard(Card card) {
		cardsInHand.remove(card);
	}
	
	
	public void displayCards() {
		System.out.println("Player " + this.getUserName() + " has the following cards in hand:");
		for(int i=0; i<cardsInHand.size(); i++) {
			System.out.println("["+i+"] " + this.cardsInHand.get(i).getName());
		}
	}

	public void rollActionDice(){
		for(int i=0; i< Config.NUM_ACTION_DICE; i++) {
			actionDice.get(i).rollDie();
		}
		
	}


	public ArrayList<Die> getDice() {
		return actionDice;
	}
	
	public ArrayList<Die> getUnusedDice() {
		ArrayList<Die> unusedDice = new ArrayList<Die>();
		
		for(Die d: actionDice) {
				unusedDice.add(d);
			}

		return unusedDice;
	}
	
	public int[] getDiceValues() {

		int[] diceValues = new int[Config.NUM_ACTION_DICE];

		for(int i=0; i< Config.NUM_ACTION_DICE; i++) {
			diceValues[i] = actionDice.get(i).getDieValue();
		}
		
		return diceValues;
	}
//	
//	//wtf is the point of this?
//	public void drawCards(int numCards) {
//		ArrayList<Card> drawnCards = board.drawCards(numCards); //returns array list of cards
//	}
	
	

	public void barfPlayerInfo(){
		System.out.println("Player " + this.getUserName());
		System.out.println("Sestertii: " + this.getSestertii());
		System.out.println("Victory Points: " + this.getVictoryPoints());
		System.out.print("ActionDice Values: ");
		this.displayActionDiceValues();
		this.displayCards();
		
	}
	
	
	//should this be moved somewhere else?
	public static void initializePlayers(Board board, UserInput userIn) throws IOException {
		String playerName;
		
		System.out.println("Welcome to Roma, the youngest player must enter their name first: ");
		for(int i=0; i<Config.NUM_PLAYERS; i++) {
			playerName = userIn.getUserInput("Enter name for player " + (i+1) + ": ");
			Player player = new Player(playerName, board, i);
			players.add(player);
		}
	}
	
	
	public static Player getCurrentPlayer() {
		int playerNum = Roma.getTurn() % Config.NUM_PLAYERS ;
		return(players.get(playerNum));
			
	}
	
	public static ArrayList<Player> getOpposingPlayers(Player currentPlayer) {
		ArrayList<Player> opposingPlayers = new ArrayList<Player>(); 
				
		for(Player p : players) {
			if(!p.equals(currentPlayer)){
				opposingPlayers.add(p);
			}
		}
		
		return(opposingPlayers);
	}
	

	public static int[] getAllPlayerVP() {
		int[] allPlayerVP = new int[Config.NUM_PLAYERS];
		
		for(int i=0; i<Config.NUM_PLAYERS; i++) {
			allPlayerVP[i] = players.get(i).getVictoryPoints();
		}
		
		return(allPlayerVP);
		
	}
	
	
	//standard getters and setters

	public int getVictoryPoints(){
		return this.victoryPoints;
	}


	public int getSestertii(){
		return this.sestertii;
	}

	public void addSestertii(int additionalSestertii){
		this.sestertii = this.sestertii+additionalSestertii;
	}

	public void subtractSestertii(int subtractedSestertii){
		this.sestertii = this.sestertii-subtractedSestertii;
	}

	public void addVictoryPoints(int additionalVictoryPoints){
		this.victoryPoints = this.victoryPoints+additionalVictoryPoints;
	}

	public void subtractVictoryPoints(int subtractedVictoryPoints){
		if(subtractedVictoryPoints > this.victoryPoints) {
			this.victoryPoints = 0;
		} else {
			this.victoryPoints = this.victoryPoints-subtractedVictoryPoints;
		}
	}

	public String getUserName() {
		return userName;
	}


	public int getPlayerNum() {
		return playerNum;
	}


	public void setPlayerNum(int playerNum) {
		this.playerNum = playerNum;
	}


	public static void setPlayer(int player) {
		players.get(player);
		
	}


	public void setSestertti(int amount) {
		this.sestertii = amount;
		
	}


	public void setVP(int points) {
		this.victoryPoints = points;
		
	}


	public void setDiceValues(int[] dice) {
		for(int i=0; i< Config.NUM_ACTION_DICE; i++) {
			actionDice.get(i).setDieValue(dice[i]);
		}	
	}
	

	public static int getOpponent(){
		int opponent;
		if (Player.getCurrentPlayer().getPlayerNum() == 0) {
			opponent = 1;
		} else {
			opponent = 0;
		}
		return opponent;
	}


	public void incrementDie(int diceChoice) {
		actionDice.get(diceChoice).incDieValue();
	}
	
	public void decrementDie(int diceChoice) {
		actionDice.get(diceChoice).decDieValue();
	}
	
}
