package Game;

import java.util.Collection;
import java.util.List;

import framework.cards.Card;

public class GameState implements framework.interfaces.GameState {

	@Override
	public int getWhoseTurn() {
		
		return Player.getPlayerNum();
	}

	@Override
	public void setWhoseTurn(int player) {
		Player.setPlayer(player);
		
	}

	@Override
	public List<Card> getDeck() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setDeck(List<Card> deck) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<Card> getDiscard() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setDiscard(List<Card> discard) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int getPlayerSestertii(int playerNum) {
		int sestertii = Player.getPlayer(playerNum).getSestertii();
		return sestertii;
	}

	@Override
	public void setPlayerSestertii(int playerNum, int amount) {
		// TODO Auto-generated method stub
		Player.getPlayer(playerNum).setSestertti(amount);
	}

	@Override
	public int getPlayerVictoryPoints(int playerNum) {
		
		return Player.getPlayer(playerNum).getVictoryPoints();
	}

	@Override
	public void setPlayerVictoryPoints(int playerNum, int points) {
		Player.getPlayer(playerNum).setVP(points);
		
	}

	@Override
	public Collection<Card> getPlayerHand(int playerNum) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void getPlayerHand(int playerNum, Collection<Card> hand) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Card[] getPlayerCardsOnDiscs(int playerNum) {
		Board.getPlayerCardsOnDiscs(playerNum);
		return null;
	}

	@Override
	public void setPlayerCardsOnDiscs(int playerNum, Card[] discCards) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int[] getActionDice() {
		return Player.getCurrentPlayer().getDiceValues();
	}

	@Override
	public void setActionDice(int[] dice) {
		Player.getCurrentPlayer().setDiceValues(dice);
		
	}

	@Override
	public int getPoolVictoryPoints() {
		// TODO Auto-generated method stub
		return 0;
	}

}
