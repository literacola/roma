package Game;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

import Cards.Card;
import static Game.Config.*;

public class Roma {

	private boolean running;
	private static int turn;
	Screen screen;
	UserInput userIn;
	private Board board;
	private RomaGameState state;

	public Roma() throws IOException {
		userIn = new UserInput(); 
	}

	public Roma(String file) throws IOException {
		userIn = new UserInput(file, this); 
	}

	public void initializeGame() throws IOException {
		running = true;
		board = new Board();
		state = new RomaGameState();
		printOpeningMessages();
		Player.initializePlayers(board, userIn);
		board.swapCards(Player.getPlayer(PLAYER1), Player.getPlayer(PLAYER2), NUM_CARDS_INIT_SWAP);  //brittle
		board.layCardsFaceDown(NUM_FACE_DOWN_CARDS, PLAYER1);
		board.layCardsFaceDown(NUM_FACE_DOWN_CARDS, PLAYER2);
		board.printBoard(false);
//		System.out.println("Player 1 now has:");
//		Player.getPlayer(Config.PLAYER1).displayCards();
//		System.out.println("Player 2 now has:");
//		Player.getPlayer(Config.PLAYER2).displayCards();
		turn=0;

	}


	public void runGame() throws IOException {
		initializeGame();
		Player currentPlayer;

		while(running) {
			currentPlayer=Player.getCurrentPlayer();
			promptUser();
			doPhase1(currentPlayer);
			doPhase2(currentPlayer);
			doPhase3();
			if(isGameOver()) {
				printGameOverMessage();
				this.stopGame();
			} else {
				nextTurn();
			}
		}
	}



	public void promptUser() {
		System.out.println("\n\n\n--------------------------------" + Player.getCurrentPlayer().getUserName() + ", you're up!--------------------------------\n\n\n");
	}
	
	
	public void doPhase1(Player currentPlayer)  {
		int numUnoccupiedDisks=0;
		System.out.println("Phase 1");
		numUnoccupiedDisks = board.getNumUnoccupiedDisks(currentPlayer.getPlayerNum());
		System.out.println(currentPlayer.getUserName() + " lost " + numUnoccupiedDisks + " victory points");
		currentPlayer.subtractVictoryPoints(numUnoccupiedDisks);
		System.out.println(currentPlayer.getUserName() + " has " + currentPlayer.getVictoryPoints() + " victory points");
		System.out.println("\n");

	}
	

	
	public void doPhase2(Player currentPlayer) {
		System.out.println("Phase 2");
		currentPlayer.rollActionDice();
		currentPlayer.displayActionDiceValues();
		System.out.println("\n");
	}

	

	public boolean isGameOver() {
		boolean result=false;
		int[] playerVP = Player.getAllPlayerVP();
		
		for(int i=0; i<playerVP.length; i++) {
			if(playerVP[i] == 0) {
				result=true;
			}
		}
		
		if(board.getNumVPLeft() == 0) {
			result=true;
		}
		
		return(result);
	}
	
	
	
	public void phase3ActivateCard(Player curPlayer, int disk) {
		Disk disks[] = board.getDiceDisks();
		
		//-1 since disks starts from zero
		disks[disk-1].getCard(curPlayer.getPlayerNum()).activate();
		
		
	}
	
	
	
	
	
	public void phase3DrawCards(Player curPlayer, int numCards) throws IOException {
		ArrayList<Card> tmpCards = new ArrayList<Card>();
		String cardSelect;
		
		tmpCards = board.drawCards(numCards);
		System.out.println("You drew: ");
		for(int i=0; i<tmpCards.size(); i++) {
				System.out.println("[" + i + "] " + tmpCards.get(i).getName());
		}
		System.out.println("Which card would you like to take?");
		cardSelect = UserInput.staticUserInput("Enter selection: ");
		curPlayer.receiveCard(tmpCards.get(Integer.parseInt(cardSelect))); //I know this is ugly, should be able to specify a type with userinput
		tmpCards.remove(tmpCards.get(Integer.parseInt(cardSelect)));
		for(Card c: tmpCards) {
			board.discard(c);
		}
		curPlayer.displayCards();
		
	}



	public void doPhase3() throws IOException {

		Player curPlayer = Player.getCurrentPlayer();
		int diceCount = 0;
		int d;
		int cardChoice;
		int locationChoice;
		ArrayList<Die> curPlayerdice = curPlayer.getDice();
		
		
		System.out.println("Phase 3");
		
		System.out.print("You currently hold ");
		if(curPlayer.getAllCardsInHand().size() == 0) {
			System.out.println(" no cards in your hand.\n");
		} else {
			System.out.println("the following cards in your hand:");
			curPlayer.displayCards();
			System.out.println("\n");
		}
		
		
		while(diceCount < Config.NUM_ACTION_DICE) {
			d = curPlayer.getDiceValues()[diceCount];
			System.out.println("What would you like to do with the " + d + " you rolled?");
			System.out.println("You can either [1] Lay Cards, [2] Take Cards, [3] Activate a card, [4] Take money, [5] Print board, [6] See wutchoo GOT");
			String choice = UserInput.staticUserInput("Enter your choice: ");
			
			
	
			
			if(choice.equals("1")){ // Lay Cards
				curPlayer.displayCards();
				System.out.println("Which card would you like to play?");
				cardChoice = UserInput.intUserInput("Enter choice: ", 0, curPlayer.getAllCardsInHand().size());
				if(curPlayer.getCard(cardChoice).getCost() > curPlayer.getSestertii()) {
					System.out.println("Not enough money, yo.");
				} else {
					curPlayer.subtractSestertii(curPlayer.getCard(cardChoice).getCost());
					System.out.println("You now have " + curPlayer.getSestertii() + " sestertii left");
					board.printBoard(false);
					System.out.println("Where would you like to place the card?");
					locationChoice = UserInput.intUserInput("Enter choice:",1,6);
					board.playCard(curPlayer.getPlayerNum(), curPlayer.playCard(cardChoice), locationChoice-1);
					board.printBoard(false);
				}
				
			} else if(choice.equals("2")) { //Draw Cards
			
				phase3DrawCards(curPlayer, d);
				diceCount++;
				curPlayerdice.get(diceCount).markUsed();
				
				
				
			} else if(choice.equals("3")) { // Activate Card
				
				if(board.getDiceDisks()[d-1].isOccupied(curPlayer.getPlayerNum())) {
					phase3ActivateCard(curPlayer, d-1);
					diceCount++;
				} else {
					System.out.println("Disk not occupied with a card, try again.");
				}
				
				
			} else if(choice.equals("4")) { //Take money
				
				curPlayer.addSestertii(d);
				System.out.println(curPlayer.getUserName() + " now has " + curPlayer.getSestertii() + " Sestertii");
				//RomaGameState state = new RomaGameState();
				//System.out.println(state.getPlayerSestertii(0));
				//System.out.println(state.getPlayerSestertii(state.getWhoseTurn()));
				//System.out.println(state.getPlayerVictoryPoints(state.getWhoseTurn()));
				//System.out.println(state.getPoolVictoryPoints());
				diceCount++;
				
			} else if(choice.equals("5")) {
				
				board.printBoard(false);
				
			} else if(choice.equals("6")) { //SEE WHATCHOO GOT
				
				curPlayer.barfPlayerInfo();
				
			} else {
				//there needs to be some way of forcing the user to re-input if they put something bad in
				System.out.println("You gave me something bad");
			}
	
		}
		
	}





	public static int getTurn() {
		return Roma.turn;
	}

	public void nextTurn() {
		turn++;
	}

	public void stopGame() {
		running = false;
	}


	public void printGameOverMessage() {
		
		int[] playerVP = Player.getAllPlayerVP();
		int mostVP=0;
		Player winningPlayer = null;
		
		for(int i=0; i<Config.NUM_PLAYERS; i++) {
			if(playerVP[i] > mostVP) {
				winningPlayer = Player.getPlayer(i);
			}
		}
		
		
System.out.println(
"   >===>          >>       >=>       >=> >=======>           >===>      >=>         >=> >=======> >======>\n"+     
">>    >=>       >>=>      >> >=>   >>=> >=>               >=>    >=>    >=>       >=>  >=>       >=>    >=>\n"+   
">=>             >> >=>     >=> >=> > >=> >=>             >=>        >=>   >=>     >=>   >=>       >=>    >=>   \n"+
">=>            >=>  >=>    >=>  >=>  >=> >=====>         >=>        >=>    >=>   >=>    >=====>   >> >==>      \n"+
">=>   >===>   >=====>>=>   >=>   >>  >=> >=>             >=>        >=>     >=> >=>     >=>       >=>  >=>     \n"+
" >=>    >>   >=>      >=>  >=>       >=> >=>               >=>     >=>       >===>      >=>       >=>    >=>   \n"+
"  >====>    >=>        >=> >=>       >=> >=======>           >===>            >=>       >=======> >=>      >=> \n");


System.out.println(winningPlayer.getUserName() + " WINS!");
		
		}
	

	public static void printOpeningMessages() {


		String title = 
						">======>         >===>      >=>       >=>       >>\n"+       
						">=>    >=>     >=>    >=>   >> >=>   >>=>      >>=>\n"+      
						">=>    >=>   >=>        >=> >=> >=> > >=>     >> >=>\n"+     
						">> >==>      >=>        >=> >=>  >=>  >=>    >=>  >=> \n"+   
						">=>  >=>     >=>        >=> >=>   >>  >=>   >=====>>=>  \n"+ 
						">=>    >=>     >=>     >=>  >=>       >=>  >=>      >=>  \n"+
						">=>      >=>     >===>      >=>       >=> >=>        >=> \n";


		System.out.println(title);

		System.out.println ("Roma - The Card Game:\n");

		//	      //Printing out components
		//	      System.out.println ("Components:");
		//	      System.out.println ("6 Dice Disks, from 1 to 6");
		//	      System.out.println ("1 Money Disk, 1 Cards Disk");
		//	      System.out.println ("36 Victory Points");
		//	      System.out.println ("6 Action Dice, 3 for each player");
		//	      System.out.println ("1 Battle die");
		//	      System.out.println ("52 Cards");
		//	      System.out.println ("");
		//	   
		//	      System.out.println ("");
		//
		//	      //Printing out initial setup
		//	      System.out.println ("Initial Setup...");
		//	      System.out.println ("6 Dice Disks are placed between the players");
		//	      System.out.println ("The Money and Cards disks are placed at each ends");
		//	      System.out.println ("Each Player recieved 10 Victory Points");
		//	      System.out.println ("Each Player selects 4 random card from the deck");
		//
		//	      //Select 2 cards to give to the other player
		//	      System.out.println ("Each Player gives the other player 2 cards face-down");
		//	      //Place each card to 1 dice disk
		//	      System.out.println ("Each Player places each card to one particular dice disk\n");
		//	      
		//	      System.out.println ("Begin Game!!!");
		//	      System.out.println ("Each player opens their cards");      
	}



}
