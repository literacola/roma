package Game;

import Cards.*;

public class Disk {

	private Card[] cardsOnDisk = new Card[Config.NUM_PLAYERS];
	private Board board;
	private int diskID;

	Disk(Board board, int diskID) {
		this.board = board;
		this.diskID = diskID;
	}

	public void playCard(Card card, int playerID) {
		if(cardsOnDisk[playerID] != null) {
			System.out.println("You are overwriting a dice disk with card: " + card.getName() + " on player " + playerID + "'s side");
			board.discard(cardsOnDisk[playerID]);
		} 
		cardsOnDisk[playerID] = card;

	}

	public void playCard(Card card, int playerID, boolean isUsingSenator) {
		cardsOnDisk[playerID] = card;	
	}


	public Card getCard(int playerID) {

		return(cardsOnDisk[playerID]);	//maybe this should return a dummy card if this is null??

	}

	public Card[] getCardsOnDisk() {
		return(this.cardsOnDisk);
	}

	//ask rupert about consillarius -- does this affect cards on the board or cards in hand?


	public boolean isOccupied(int playerID) {
		boolean result=false;

		if(cardsOnDisk[playerID] != null) {
			result=true;
		}

		return(result);
	}

	public boolean isOccupiedAtAll() {
		boolean result=false;

		for(int i=0; i<Config.NUM_PLAYERS; i++) {
			if(cardsOnDisk[i] != null) {
				result=true;
			}
		}
		return(result);
	}


	//wtf is this??
	public int getNumUnoccupied(int playerID) {
		int numUnoccupied=0;

		//System.out.println("getting unoccupied disks for player: " + playerID);

		if(cardsOnDisk[playerID] == null) {
			numUnoccupied++;
		}

		return(numUnoccupied);
	}

	public void printDisk() {

		System.out.print("Disk:" + this.getDiskID());

	}


	public void printCardsOnDisk() {
		System.out.println("Cards on disk:");
		for(int i=0; i<Config.NUM_PLAYERS; i++){

			if(this.cardsOnDisk[i] != null) {
				System.out.println("Player " + i + ": " + cardsOnDisk[i].getName());
			}
		}	
	}

	public int getDiskID() {
		return(this.diskID);
	}

	public void removeCard(int playerNum) {

		if(playerNum <= Config.NUM_PLAYERS){
			board.discard(cardsOnDisk[playerNum]);
			cardsOnDisk[playerNum] = null;
		}else{
			System.err.println("Bad input to remove card" + " " + playerNum);
		}
	}


}