package Game;

import java.util.Collection;
import java.util.List;

import framework.cards.Card;

public class RomaGameState implements framework.interfaces.GameState {

	private Board board;
	private Player player;
	
	public RomaGameState() {
		board = new Board();
	}
	
	@Override
	public int getWhoseTurn() {
		
		//return Player.getPlayerNum();
		return Player.getCurrentPlayer().getPlayerNum();
	}

	@Override
	public void setWhoseTurn(int player) {
		Player.setPlayer(player);
		
	}

	@Override
	public List<Card> getDeck() {
		// TODO Auto-generated method stub
		//return board.getDeck();
		return null;
	}

	@Override
	public void setDeck(List<Card> deck) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<Card> getDiscard() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setDiscard(List<Card> discard) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int getPlayerSestertii(int playerNum) {
		int sestertii = Player.getPlayer(playerNum).getSestertii();
		return sestertii;
	}

	@Override
	public void setPlayerSestertii(int playerNum, int amount) {
		// TODO Auto-generated method stub
		Player.getPlayer(playerNum).setSestertti(amount);
	}

	@Override
	public int getPlayerVictoryPoints(int playerNum) {
		
		return Player.getPlayer(playerNum).getVictoryPoints();
	}

	@Override
	public void setPlayerVictoryPoints(int playerNum, int points) {
		Player.getPlayer(playerNum).setVP(points);
		
	}

	@Override
	public Collection<Card> getPlayerHand(int playerNum) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void getPlayerHand(int playerNum, Collection<Card> hand) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Card[] getPlayerCardsOnDiscs(int playerNum) {
		// TODO Auto-generated method stub
		Board.getPlayerCardsOnDiscs(playerNum);
		return null;
	}

	@Override
	public void setPlayerCardsOnDiscs(int playerNum, Card[] discCards) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int[] getActionDice() {
		return Player.getCurrentPlayer().getDiceValues();
	}

	@Override
	public void setActionDice(int[] dice) {
		Player.getCurrentPlayer().setDiceValues(dice);
		
	}

	@Override
	public int getPoolVictoryPoints() {
		return board.getNumVPLeft();
	}

}
