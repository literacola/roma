package Cards;

import java.io.IOException;

import framework.interfaces.activators.CardActivator;

import Game.Board;
import Game.Config;
import Game.Disk;
import Game.Player;


//should this be abstract?
public abstract class Card {
	private String name;
	private String description;
	private int cost;
	private int defense;
	private int type;
	private final int origDefense;
	private boolean defenseModified;
	private framework.cards.Card enumName;
	private CardActivator frameworkActivator;
	
	
	public Card(String name, String description, int cost, int defense, int type, framework.cards.Card enumName) {
		this.name = name;
		this.description = description;
		this.cost=cost;
		this.defense=defense;
		this.type=type;
		this.origDefense = defense;
		this.defenseModified = false;
		this.enumName = enumName;
	}
	
	public int getDefense() {
		return defense;
	}
	
	public int getCost() {
		return cost;
	}
	
	public String getDescription() {
		return description;
	}
	
	public String getName() {
		return name;
	}
	
	public int getType() {
		return type;
	}
	
	public void decrementDefense(int num) {
		defense-=num;
		assert(defense >= 0);
	}
	
	public void activate() throws IOException {

	}

	public void activate(int location) throws IOException {

	}

	public void activate(Cards.Card myCard, int locationChoice) {	

	}

	public void activate(int actionDieValue, int changeAmt) {
		
	}
	
   public void activate(int attackValue, boolean repeatAttack,
         int actionDieValue) {
      
   }
	
//	public int[] getPosition(Card card, Disk[] disks) {
//		int[] pos = new int[2];
//
//		for(int i=0; i<disks.length; i++ ) {
//			for(int j=0; j<Config.NUM_PLAYERS; j++) {
//				if(disks[i].getCard(j) != null) {
//					if(disks[i].getCard(j).equals(card)) {
//						pos[0] = i; //which dice disk
//						pos[1] = j; //which player
//					}
//				}
//			}
//		}		
//		return pos;	
//	}
	
	public int[] getPosition(Card card, Disk[] disks) {
//		Player curPlayer = Player.getCurrentPlayer();
	   Player curPlayer = Player.getPlayer(Player.getTestPlayer());
		int[] pos = new int[2];

//		pos[0] = curPlayer.getCurrentActionDie().getDieValue()-1;
//		pos[1] = curPlayer.getPlayerNum();
		pos[0] = curPlayer.getCurrentDiskPosition();
		pos[1] = curPlayer.getPlayerNum();
		return pos;	
	}
	
	public boolean isDefenseModified() {
		return defenseModified;
	}
	
	public void setDefenseModified() {
		defenseModified = true;
	}
	
	public void resetDefenseModified() {
		defenseModified = false;
		this.defense = this.origDefense;
	}
	
	public framework.cards.Card getEnum() {
		return this.enumName;
	}
	
	public abstract framework.interfaces.activators.CardActivator getActivator(int disc);





}
