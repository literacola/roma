package Cards;

import java.io.IOException;
import java.util.ArrayList;

import framework.interfaces.activators.CardActivator;

import CardImplimentation.MyArchitectusActivator;
import CardImplimentation.MySenatorActivator;
import Game.Board;
import Game.Config;
import Game.Player;
import Game.UserInput;

public class Architectus extends Card{

   private Board board;

   public Architectus(Board board) {
      //name, description, cost, defense, type
      super("Architectus", "Lay building cards Free and cover any cards",3,4,Config.CHARACTER,framework.cards.Card.ARCHITECTUS);
      this.board = board;
   }


   public void activate() throws IOException {
      int currPlayerID = Player.getCurrentPlayer().getPlayerNum();
      Player currPlayer = Player.getCurrentPlayer();
      ArrayList<Card>allCards = currPlayer.getAllCardsInHand();

      if(allCards.isEmpty()) {
         System.out.println("You don't have any cards to lay");
      } else {

         board.printBoard(false);

         for(int i=0; i<currPlayer.getAllCardsInHand().size(); i++) {
            System.out.println("["+ i +"] " + currPlayer.getAllCardsInHand().get(i).getName());
         }

         int cardChoice = UserInput.intUserInput("Which card would you like to lay?", 0, currPlayer.getAllCardsInHand().size());
         int posChoice =  UserInput.intUserInput("Where would you like to play this card?", 1, Config.NUM_DICEDISKS);
         board.playCard(currPlayerID, currPlayer.getAllCardsInHand().get(cardChoice), posChoice-1);
         while(cardChoice != -1 && currPlayer.getAllCardsInHand().size() != 0) {

            System.out.println("size is:"+ currPlayer.getAllCardsInHand().size());

            currPlayer.displayCards();

            board.playCard(currPlayerID, currPlayer.getAllCardsInHand().get(cardChoice), posChoice-1);
            for(int i=0; i<allCards.size(); i++) {
               System.out.println("["+ i +"] " + currPlayer.getAllCardsInHand().get(i).getName());
            }
            System.out.println("[-1] Exit");

            cardChoice = UserInput.intUserInput("Which card would you like to lay?", -1, allCards.size());
            posChoice =  UserInput.intUserInput("Where would you like to play this card?", 1, Config.NUM_DICEDISKS);	

         }

         System.out.println("You played a architectus!");
      }
   }




   @Override
   public CardActivator getActivator(int disc) {
      CardActivator activator = new MyArchitectusActivator(disc);
      return activator;
   }

   public void layCard (Card card, int location) {
      int playerID = Player.getTestPlayer();
      board.playCard(playerID, card, location-1);
      Player.getPlayer(playerID).removeCard(card.getEnum());
   }
}