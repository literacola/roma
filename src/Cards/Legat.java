package Cards;
import CardImplimentation.MyLegatActivator;
import Game.Board;
import Game.Config;
import Game.Disk;
import Game.Player;





public class Legat extends Card {
	
	Board board;
	
	public Legat(Board board) {
		//name, description, cost, defense, type
		super("Legat", "Gets 1 vp from the stockpile for every unoccupied dice disk",5,2,Config.CHARACTER,framework.cards.Card.LEGAT);
		this.board = board;
	}
	
	public void activate(){
		
		Disk[] diceDisk = board.getDiceDisks();
		int numVP;
		int[] pos = super.getPosition(this, diceDisk);
		int currentTestPlayer = Player.getTestPlayer();
		
		
		if(pos[1] == 0) { 
			numVP=board.getNumUnoccupiedDisks(1);
		} else {
			numVP=board.getNumUnoccupiedDisks(0);
		}
		
		int points = Player.getPlayer(currentTestPlayer).getVictoryPoints();
		Player.getPlayer(currentTestPlayer).setVP((points + numVP));
		System.out.println("You just played a Legat! You got an extra " + numVP + " VP!");
		
	}
	
	public void activate(int position){
		
		Disk[] diceDisk = board.getDiceDisks();
		int numVP;
		int[] pos = super.getPosition(this, diceDisk);
		int currentTestPlayer = Player.getTestPlayer();

		if(pos[1] == 0) {  
			numVP=board.getNumUnoccupiedAndUnblockedDisks(1);
		} else {
			numVP=board.getNumUnoccupiedAndUnblockedDisks(0);
		}
		
		int points = Player.getPlayer(currentTestPlayer).getVictoryPoints();
		Player.getPlayer(currentTestPlayer).setVP((points + numVP));
		
	}
	
	
	public framework.interfaces.activators.CardActivator getActivator(int disc) {
		MyLegatActivator Activator = new CardImplimentation.MyLegatActivator(disc);
		
		return Activator;
	}
	
}

