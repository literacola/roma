package Cards;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;

import framework.interfaces.activators.CardActivator;

import CardImplimentation.MyLegatActivator;
import CardImplimentation.MySicariusActivator;
import Game.*;


public class Sicarius extends Card {

	private Board board;
	
	public Sicarius(Board board) {
		//name, description, cost, defense, type
		super("Sicarius", "Discards opposing character card and this card",9,2,Config.CHARACTER,framework.cards.Card.SICARIUS);
		this.board = board;
	}
	
	
	public void activate() throws IOException {
		
		Disk[] diceDisk = board.getDiceDisks();
		int pos[] = super.getPosition(this, diceDisk);
		//pos[0] = dice disk
		//pos[1] = player
		int diskPos = pos[Config.DICE_DISK_POS];
		int playerPos = pos[Config.PLAYER_POS];

		Card chosenCard;

		int opponentPlayerID = Player.getOpponent();
		Player opponentPlayer = Player.getPlayer(opponentPlayerID);
		ArrayList<Disk> occupiedDisks = board.getOccupiedDisks(opponentPlayerID);
		diceDisk[diskPos].removeCard(playerPos);

		for(int i=0; i < occupiedDisks.size(); i++) {
			if(occupiedDisks.get(i).getCard(Player.getCurrentPlayer().getPlayerNum()).getType() == Config.CHARACTER) {
				System.out.println("[" + (occupiedDisks.get(i).getDiskID()+1) + "]: " + occupiedDisks.get(i).getCard(opponentPlayerID).getName());
			}
		}

      int diskChoice = 0;

      diskChoice = UserInput.intUserInput("which card do you want to choose ", 1, Config.NUM_DICEDISKS);  //magic numbers are bad mkay
   
      
      chosenCard = diceDisk[diskChoice-1].getCard(opponentPlayerID);

      board.discard(chosenCard);
      diceDisk[diskChoice-1].removeCard(opponentPlayerID);

      System.out.println("You just played a Sicarius");
		
	}

	public void activate(int target, int location) {

	   Disk[] diceDisk = board.getDiceDisks();
	   int pos[] = super.getPosition(this, diceDisk);
	   //pos[0] = dice disk
	   //pos[1] = player
	   int diskPos = pos[Config.DICE_DISK_POS];
	   int playerPos = pos[Config.PLAYER_POS];

	   Card chosenCard;
	   int targetChoice = target;
	   int curLocation = diskPos;
	   int opponentPlayerID = Player.getOpposingTestPlayer();
	   chosenCard = diceDisk[targetChoice-1].getCard(opponentPlayerID);
	   if (chosenCard.getType() == Config.CHARACTER) {
	   
	   diceDisk[curLocation].removeCard(Player.getTestPlayer());

	   diceDisk[targetChoice-1].removeCard(opponentPlayerID);
	   }

	}
	
	@Override
	public CardActivator getActivator(int disc) {
		MySicariusActivator activator = new MySicariusActivator(disc);
		return activator;
	}
		
}
