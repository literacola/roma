package Cards;
import framework.interfaces.activators.CardActivator;
import CardImplimentation.MyTribunusPlebisActivator;
import Game.Board;
import Game.Config;
import Game.Disk;
import Game.Player;

import static Game.Config.*;

public class TribunusPlebis extends Card {

	private Board board;

	public TribunusPlebis(Board board) {
		//name, description, cost, defense, type
		super("Tribunus", "Get one VP from opponent",5,5,Config.CHARACTER,framework.cards.Card.TRIBUNUSPLEBIS);
		this.board = board;
	}


	public void activate() {
		


		Disk[] diceDisk = board.getDiceDisks();
		int[] pos = super.getPosition(this, diceDisk);

		if(pos[PLAYER_POS] == PLAYER1 ) {
			if(Player.getPlayer(PLAYER2).getVictoryPoints() > 0) {
				Player.getPlayer(PLAYER1).addVictoryPoints(1);
				Player.getPlayer(PLAYER2).subtractVictoryPoints(1);
			} else {
				System.out.println("Opponent doesn't have any victory points to take");
			}
		} else if(pos[PLAYER_POS] == PLAYER2 ) {
			if(Player.getPlayer(PLAYER1).getVictoryPoints() > 0) {
				Player.getPlayer(PLAYER2).addVictoryPoints(1);					//sniff sniff, smells like repeated code :(
				Player.getPlayer(PLAYER1).subtractVictoryPoints(1);
			} else {
				System.out.println("Opponent doesn't have any victory points to take");
			}
		}
		
		
		System.out.println("You played a Tribunus Plebis! You just got 1 VP from your opponent!");
	}


	@Override
	public CardActivator getActivator(int disc) {
		CardActivator activator = new MyTribunusPlebisActivator(disc);
		return activator;
	}
}
