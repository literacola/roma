package Cards;

import java.io.IOException;
import java.util.ArrayList;

import framework.interfaces.activators.CardActivator;

import CardImplimentation.MyScaenicusActivator;
import Game.Board;
import Game.Config;
import Game.Disk;
import Game.Player;
import Game.UserInput;

public class Scaenicus extends Card {

	private Board board;

	public Scaenicus(Board board) {
		//name, description, cost, defense, type
		super("Scaenicus", "Copies other card",8,1,Config.CHARACTER,framework.cards.Card.SCAENICUS);
		this.board = board;
	}


	public void activate() throws IOException {
		//NEED TO MAKE SURE NEXT ROUND, THE SAME CARD WONT BE USED
		Disk[] diceDisk = board.getDiceDisks();
		int pos[] = super.getPosition(this, diceDisk);
		//pos[0] = dice disk
		//pos[1] = player
		int diskPos = pos[Config.DICE_DISK_POS];
		int playerPos = pos[Config.PLAYER_POS];
		Card chosenCard;

		Player currentPlayer = Player.getCurrentPlayer();
		int currentPlayerID = currentPlayer.getPlayerNum();

		ArrayList<Disk> occupiedDisks = board.getOccupiedDisks(currentPlayerID);

		for(int i=0; i < occupiedDisks.size(); i++) {
			System.out.println("[" + (occupiedDisks.get(i).getDiskID() + 1) + "]: " + occupiedDisks.get(i).getCard(currentPlayerID).getName());
		}

		int diskChoice = 0;
		diskChoice = UserInput.intUserInput("which card do you want to choose",0,diceDisk.length-1);
		

		chosenCard = diceDisk[diskChoice-1].getCard(currentPlayerID);

		chosenCard.activate();

		System.out.println("You just played a Scaenicus");

	}

	@Override
	public CardActivator getActivator(int disc) {
		CardActivator activator = new MyScaenicusActivator(disc);
		return activator;
	}
	
}
