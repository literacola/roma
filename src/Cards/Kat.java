package Cards;

import framework.interfaces.activators.CardActivator;
import CardImplimentation.MyLegatActivator;
import Game.Board;
import Game.Config;
import Game.Disk;
import Game.Player;

public class Kat extends Card {
	
	Board board;
	private int life;
	
	public Kat(Board board) {
		//name, description, cost, defense, type
		super("Kat", "Mysterious and revered animal (kat).  Has nine lives.",5,1,Config.CHARACTER,framework.cards.Card.KAT);//CHANGE THIS TO KATTTT
		this.board = board;
		this.life = Config.KAT_LIVES;
	}
	
	public void activate(){
		System.out.println("miaow");
	}
	
	public int getLives() {
		return this.life;
	}
	
	public void killOnce() {
		this.life--;
	}

	public CardActivator getActivator(int disc) {
		return null;
	}

}


