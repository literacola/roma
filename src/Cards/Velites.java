package Cards;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

import framework.interfaces.activators.CardActivator;

import CardImplimentation.MyVelitesActivator;
import Game.Board;
import Game.Config;
import Game.Disk;
import Game.Player;
import Game.UserInput;

public class Velites extends Card {

   private Board board;

   public Velites(Board board) {
      //name, description, cost, defense, type, type
      super("Velites", "Attacks opponents character card",5,3,Config.CHARACTER,framework.cards.Card.VELITES);
      this.board = board;
   }


   public void activate() throws IOException {

      System.out.println("You just played a Velites");


      Disk[] diceDisk = board.getDiceDisks();

      Card chosenCard;

      int opponentPlayerID = Player.getOpponent();
      ArrayList<Disk> occupiedDisks = board.getOccupiedDisks(opponentPlayerID);

      System.out.println("You just played a Velites! Choose card to attack!");

      for(int i=0; i < occupiedDisks.size(); i++) {
         System.out.println("[" + (i+1) + "]: " + occupiedDisks.get(i).getCard(opponentPlayerID).getName());
      }

      int diskChoice = 0;

      diskChoice = UserInput.intUserInput("which card do you want to choose", 1,Config.NUM_DICEDISKS);  //magic numbers are bad mkay


      chosenCard = diceDisk[diskChoice-1].getCard(opponentPlayerID);

      Random rand = new Random();
      int dieValue = Math.abs(((rand.nextInt()) % Config.NUM_FACES_DIE))+1;

      System.out.println("Battle Die Value: " + dieValue);



      int[] attackedPos = chosenCard.getPosition(chosenCard, board.getDiceDisks());

      if (dieValue >= chosenCard.getDefense()) {
         diceDisk[attackedPos[Config.DICE_DISK_POS]].removeCard(opponentPlayerID);
         System.out.println("In a flash of steel, you have vanquished your opponents " + chosenCard.getName() + "!");
      } else {
         System.out.println("You have failed to kill your opponent. You leave the battleground wearily dragging your sword.");
      }



   }

   public void activate(int target, int battleDieValue) {		

      Disk[] diceDisk = board.getDiceDisks();

      Card chosenCard;

      int opponentPlayerID = Player.getOpposingTestPlayer();
      ArrayList<Disk> occupiedDisks = board.getOccupiedDisks(opponentPlayerID);


      int diskChoice = target;	

      chosenCard = diceDisk[diskChoice-1].getCard(opponentPlayerID);

      int dieValue = battleDieValue;		

      if (dieValue >= chosenCard.getDefense()) {
         diceDisk[diskChoice-1].removeCard(opponentPlayerID);
      } 
   }

   @Override
   public CardActivator getActivator(int disc) {
      CardActivator activator = new MyVelitesActivator(disc);
      return activator;
   }
}


