package Cards;

import java.io.IOException;
import java.util.ArrayList;

import framework.interfaces.activators.CardActivator;
import framework.interfaces.activators.NeroActivator;
import CardImplimentation.MyNeroActivator;
import Game.Board;
import Game.Config;
import Game.Disk;
import Game.Player;
import Game.UserInput;

public class Nero extends Card{

   private Board board;

   public Nero(Board board) {
      //name, description, cost, defense, type
      super("Nero", "Discards opposing building card and this card",8,9,Config.CHARACTER,framework.cards.Card.NERO);
      this.board = board;
   }


   public void activate() throws IOException {

      Disk[] diceDisk = board.getDiceDisks();
      int pos[] = super.getPosition(this, diceDisk);
      //pos[0] = dice disk
      //pos[1] = player
      int diskPos = pos[Config.DICE_DISK_POS];
      int playerPos = pos[Config.PLAYER_POS];

      Card chosenCard;

      int opponentPlayerID = Player.getOpponent();
      Player opponentPlayer = Player.getPlayer(opponentPlayerID);
      ArrayList<Disk> occupiedDisks = board.getOccupiedDisks(opponentPlayerID);

      diceDisk[diskPos].removeCard(playerPos);

      for(int i=0; i < occupiedDisks.size(); i++) {
         if(occupiedDisks.get(i).getCard(Player.getCurrentPlayer().getPlayerNum()).getType() == Config.BUILDING) {
            System.out.println("[" + (occupiedDisks.get(i).getDiskID()+1) + "]: " + occupiedDisks.get(i).getCard(opponentPlayerID).getName());
         }
      }

      int diskChoice = 0;

      diskChoice = UserInput.intUserInput("which card do you want to choose ", 1, Config.NUM_DICEDISKS);
      
      chosenCard = diceDisk[diskChoice-1].getCard(opponentPlayerID);

      if(chosenCard.getType() == Config.BUILDING) {
         board.discard(chosenCard);
         diceDisk[diskChoice-1].removeCard(opponentPlayerID);
      }
      System.out.println("You just played a Nero");

   }

   public void activate(int location) throws IOException {



      Disk[] diceDisk = board.getDiceDisks();
      int pos[] = super.getPosition(this, diceDisk);
      //pos[0] = dice disk
      //pos[1] = player
      int diskPos = pos[Config.DICE_DISK_POS];
      int playerPos = pos[Config.PLAYER_POS];

      Card chosenCard;
      int diskChoice = location;


      int opponentPlayerID = Player.getOpposingTestPlayer();
      chosenCard = diceDisk[diskChoice-1].getCard(opponentPlayerID);
      if (chosenCard.getType() == Config.BUILDING) {

         diceDisk[diskPos].removeCardByNero(Player.getTestPlayer());

         diceDisk[diskChoice-1].removeCardByNero(opponentPlayerID);
      }
   }

   @Override
   public CardActivator getActivator(int disc) {
      MyNeroActivator activator = new MyNeroActivator(disc);
      return activator;
   }
}
