package Cards;

import framework.interfaces.activators.CardActivator;
import Game.*;

public class Machina extends Card{
Board board;
	
	public Machina(Board board) {
		//name, description, cost, defense, type
		super("Machina", "Does nothing",4,4,Config.BUILDING,framework.cards.Card.MACHINA);
		this.board = board;
	}
	
	public void activate(){
		
	}

	@Override
	public CardActivator getActivator(int disc) {
		// TODO Auto-generated method stub
		return null;
	}
}
