package Cards;

import framework.interfaces.activators.CardActivator;
import Game.*;

public class Aesculapinum extends Card{
Board board;
	
	public Aesculapinum(Board board) {
		//name, description, cost, defense, type
		super("Aesculapinum", "Does nothing",5,2,Config.BUILDING,framework.cards.Card.AESCULAPINUM);
		this.board = board;
	}
	
	public void activate(){
		
	}

	@Override
	public CardActivator getActivator(int disc) {
		// TODO Auto-generated method stub
		return null;
	}
	
}
