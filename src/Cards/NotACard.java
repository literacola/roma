package Cards;

import framework.interfaces.activators.CardActivator;
import Game.Board;
import Game.Config;
import Game.Disk;
import Game.Player;

public class NotACard extends Card{
Board board;
	
	public NotACard(Board board) {
		//name, description, cost, defense, type
		super("Not A Card", "Does nothin",0,0,Config.NOT_A_CARD,framework.cards.Card.NOT_A_CARD);
		this.board = board;
	}
	
	public void activate(){
		
		System.out.println("Youve activated not a card....");
		
	}

	@Override
	public CardActivator getActivator(int disc) {
		return null;
	}
}
