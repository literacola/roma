package Cards;

import java.io.IOException;
import java.util.ArrayList;

import framework.interfaces.activators.CardActivator;

import CardImplimentation.MyConsulActivator;
import Game.Board;
import Game.Config;
import Game.Die;
import Game.Player;
import Game.UserInput;

public class Consul extends Card{

   Board board;

   public Consul(Board board) {
      //name, description, cost, defense, type
      super("Consul", "increase/decrease action dice value by 1",3,3,Config.CHARACTER,framework.cards.Card.CONSUL);
      this.board = board;
   }

   public void activate() {
      int diceChoice = 0;
      int incOrDec = 0;
      int[] unusedDieIndex = Player.getCurrentPlayer().getUnusedDiceIndex();


      ArrayList<Die> unusedDice = Player.getCurrentPlayer().getUnusedDice();


      for(int i = 0; i<unusedDice.size(); i++){
         System.out.println("[" + i + "]: " + unusedDice.get(i).getDieValue());
      }

      try {
         diceChoice = UserInput.intUserInput("Which die would you like to use? ",0,unusedDice.size());
      } catch (IOException e) {
         // TODO Auto-generated catch block
         e.printStackTrace();
      }

      try {
         incOrDec = UserInput.intUserInput("Do you want to increase or decrease - 1 for increase, 0 for decrease? ",0,1);
      } catch (IOException e) {
         // TODO Auto-generated catch block
         e.printStackTrace();
      }

      diceChoice = unusedDieIndex[diceChoice];

      if(incOrDec == 1){
         Player.getCurrentPlayer().incrementDie(diceChoice);
      } else {
         Player.getCurrentPlayer().decrementDie(diceChoice);
      }


   }

   public void activate(int actionDieValue, int changeAmt) {

      int diceChoice = 0;
      int incOrDec = 0;
      Player curPlayer = Player.getPlayer(Player.getTestPlayer());

      incOrDec = changeAmt;
      diceChoice = actionDieValue;
      int indexChoice = 0;

      ArrayList<Die> unusedDice = curPlayer.getUnusedDice();


      for(int i = 0; i<curPlayer.getDice().size(); i++){
         if((curPlayer.getDice().get(i).getDieValue() == actionDieValue) && (!curPlayer.getDice().get(i).isUsed())) {
            indexChoice = i;
         }
      }

      if(incOrDec == 1){
         curPlayer.incrementDie(indexChoice);
      } else if (incOrDec == -1){
         curPlayer.decrementDie(indexChoice);
      }

   }

   @Override
   public CardActivator getActivator(int disc) {

      MyConsulActivator activator = new MyConsulActivator(disc);
      return activator;
   }
}
