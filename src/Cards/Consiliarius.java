package Cards;

import java.io.IOException;
import java.util.ArrayList;

import framework.interfaces.activators.CardActivator;

import CardImplimentation.MyConsiliariusActivator;
import CardImplimentation.MySicariusActivator;
import Game.Board;
import Game.Config;
import Game.Disk;
import Game.Player;
import Game.UserInput;

public class Consiliarius extends Card{

   private Board board;
   ArrayList<Card> cardList = new ArrayList<Card>();
   ArrayList<Integer> cardIndex = new ArrayList<Integer>();


   public Consiliarius(Board board) {
      //name, description, cost, defense, type
      super("Consiliarius", "pick character cards and lay to dice disk, cover building",4,4,Config.CHARACTER,framework.cards.Card.CONSILIARIUS);
      this.board = board;

   }

   public void activate() throws IOException{

      Player curPlayer = Player.getCurrentPlayer();
      int playerID = curPlayer.getPlayerNum();
      int cardChoice = 0;
      int locationChoice = 0;
      int numAdded = 0;

      Disk[] diskList = board.getDiceDisks();
      int i=0;
      for (i=0; i<diskList.length; i++) {
         if(diskList[i].getCard(playerID).getType() == Config.CHARACTER) {
            cardList.add(diskList[i].pickUpCard(playerID));
            cardIndex.add(i);
            numAdded++;
         }
      }
      int choice = 0;
      while (choice != -1) {
         for (int j = 0; j < cardList.size(); j++) {
            System.out.println("[" + j + "] " + cardList.get(j).getName());
         }
         choice = UserInput.intUserInput("which card do you want to lay? [-1] to be done",-1,i);
         if(choice != -1){
            board.printBoard(false);
            int location = UserInput.intUserInput("where do you want to place it? ", 1, Config.NUM_DICEDISKS);
            board.playCard(Player.getTestPlayer(), cardList.get(choice), location-1);

            cardList.remove(choice);
            cardIndex.remove(choice);
         }
      }
      returnOtherCards(playerID);
      
      System.out.println("You just played a Consiliarius!");

   }

   public void activate(int location) {

      int playerID = Player.getTestPlayer();
      Player curPlayer = Player.getPlayer(playerID);
      int locationChoice = location;     


      Disk[] diskList = board.getDiceDisks();

      for (int i=0; i<diskList.length; i++) {
         if(diskList[i].getCard(playerID).getType() == Config.CHARACTER) {
            cardList.add(diskList[i].pickUpCard(playerID));
            cardIndex.add(i);
         }
      }

   }

   public void placeCard(Card c, int location ) {
      int removeMe=-1;

      location -= 1;
      for (int i=0; i<cardList.size(); i++) {
         if(c.getEnum() == cardList.get(i).getEnum()) {
            removeMe=i;
         }
      }

      board.playCard(Player.getTestPlayer(), cardList.get(removeMe), location);

      if(removeMe != -1) {
         cardList.remove(removeMe);
         cardIndex.remove(removeMe);
      }

   }


   public CardActivator getActivator(int disc) {
      MyConsiliariusActivator Activator = new MyConsiliariusActivator (disc);
      return Activator;
   }

   public void returnOtherCards() {
      int playerID = Player.getTestPlayer();
      for (int i=0; i<cardList.size(); i++) {
         int location = cardIndex.get(i);
         if(board.getDiceDisks()[location].getCard(playerID).getType() != Config.CHARACTER){
            board.playCard(Player.getTestPlayer(), cardList.get(i), location);
            cardList.remove(i);
            cardIndex.remove(i);
         }
      }

   }
   
   public void returnOtherCards(int playerID) {
      for (int i=0; i<cardList.size(); i++) {
         int location = cardIndex.get(i);
         if(board.getDiceDisks()[location].getCard(playerID).getType() != Config.CHARACTER){
            board.playCard(Player.getTestPlayer(), cardList.get(i), location);
            cardList.remove(i);
            cardIndex.remove(i);
         }
      }

   }


}
