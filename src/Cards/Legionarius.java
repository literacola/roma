package Cards;

import java.util.Random;

import framework.interfaces.activators.CardActivator;

import CardImplimentation.MyLegionariusActivator;
import CardImplimentation.MySicariusActivator;
import Game.Board;
import Game.Config;
import Game.Disk;
import Game.Player;

public class Legionarius extends Card{

	private Board board;

	public Legionarius(Board board) {
		//name, description, cost, defense, type
		super("Legionarius", "Attacks opposing card",4,5,Config.CHARACTER,framework.cards.Card.LEGIONARIUS);
		this.board = board;
	}


	public void activate() {

		Disk[] diceDisk = board.getDiceDisks();
		int pos[] = super.getPosition(this, diceDisk);
		//pos[0] = dice disk
		//pos[1] = player
		int diskPos = pos[Config.DICE_DISK_POS];
		int opponentID = Player.getOpponent();

		Card opposingCard = diceDisk[diskPos].getCard(opponentID);
		if(opposingCard.getEnum() != framework.cards.Card.NOT_A_CARD) {
			int defValue = opposingCard.getDefense();

			Random rand = new Random();
			int dieValue = Math.abs(((rand.nextInt()) % Config.NUM_FACES_DIE))+1;

			System.out.println("Battle Die Value: " + dieValue);

			if (dieValue >= defValue) {
				diceDisk[diskPos].removeCard(opponentID);
				System.out.println("you have removed " + opposingCard.getName());
			} else {
				System.out.println("You have failed to kill your opponent.");
			}
		}
		System.out.println("You just played a Legionarius");

	}
	
	public void activate(int battleDieValue) {

		Disk[] diceDisk = board.getDiceDisks();
		int pos[] = super.getPosition(this, diceDisk);
		//pos[0] = dice disk
		//pos[1] = player
		int diskPos = pos[Config.DICE_DISK_POS];
		int opponentID = Player.getOpposingTestPlayer();

		Card opposingCard = diceDisk[diskPos].getCard(opponentID);
		if(opposingCard.getEnum() != framework.cards.Card.NOT_A_CARD) {
			int defValue = opposingCard.getDefense();

			int dieValue = battleDieValue;

			if (dieValue >= defValue) {
				diceDisk[diskPos].removeCard(opponentID);
			}
		}

	}


	@Override
	public CardActivator getActivator(int disc) {
		MyLegionariusActivator activator = new MyLegionariusActivator(disc);
		return activator;
	}
}
