package Cards;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

import framework.interfaces.activators.CardActivator;

import CardImplimentation.MyCenturioActivator;
import Game.Board;
import Game.Config;
import Game.Die;
import Game.Disk;
import Game.Player;
import Game.UserInput;

public class Centurio extends Card{

   private Board board;

   public Centurio(Board board) {
      //name, description, cost, defense
      super("Centurio", "Attacks opposing card with addition of action dice",9,5,Config.CHARACTER, framework.cards.Card.CENTURIO);
      this.board = board;
   }


   public void activate() {

      Disk[] diceDisk = board.getDiceDisks();
      int pos[] = super.getPosition(this, diceDisk);
      //pos[0] = dice disk
      //pos[1] = player
      int diskPos = pos[Config.DICE_DISK_POS];
      int opponentID = Player.getOpponent();

      Card opposingCard = diceDisk[diskPos].getCard(opponentID);
      if(opposingCard.getEnum() != framework.cards.Card.NOT_A_CARD) {
         int defValue = opposingCard.getDefense();

         Random rand = new Random();
         int dieValue = Math.abs(((rand.nextInt()) % Config.NUM_FACES_DIE))+1;

         System.out.println("Battle Die Value: " + dieValue);

         int addActionDice = 0;
         if (!Player.getCurrentPlayer().getUnusedDice().isEmpty()) {
            try {
               addActionDice = UserInput.intUserInput("Do you want to add a value of an unused action die? 1=yes 0=no: ");
            } catch (IOException e) {
               e.printStackTrace();
            }
         } else {
            System.out.println("You got no action die left.");
         }
         if (addActionDice == 1) {
            ArrayList<Die> unusedDie = Player.getCurrentPlayer().getUnusedDice();

            System.out.println("Action Die Available");
            for (int i=0; i<unusedDie.size(); i++) {
               System.out.println("[" + i + "]: " + unusedDie.get(i).getDieValue());
            }

            int choice = 0;
            try {
               choice = UserInput.intUserInput("enter choice: ");
            } catch (IOException e) {
               // TODO Auto-generated catch block
               e.printStackTrace();
            }
            dieValue += unusedDie.get(choice).getDieValue();
            Player.getCurrentPlayer().getUnusedDice().get(choice).markUsed();
         }
         if (dieValue >= defValue) {
            diceDisk[diskPos].removeCard(opponentID);
            System.out.println("you have removed " + opposingCard.getName());
         } else {
            System.out.println("You have failed to kill your opponent.");
         }
      }

      System.out.println("You just played a Centurio");

   }

   public void activate(int attackValue, boolean repeatAttack, int actionDieValue) {

      Disk[] diceDisk = board.getDiceDisks();
      int pos[] = super.getPosition(this, diceDisk);
      //pos[0] = dice disk
      //pos[1] = player
      int diskPos = pos[Config.DICE_DISK_POS];
      int opponentID = Player.getOpposingTestPlayer();
      int currentTestPlayer = Player.getTestPlayer();

      Card opposingCard = diceDisk[diskPos].getCard(opponentID);

      if(opposingCard.getEnum() != framework.cards.Card.NOT_A_CARD) {
         int defValue = opposingCard.getDefense();

         int dieValue = attackValue;

         boolean addActionDice = repeatAttack;

         if (addActionDice) {

            dieValue += actionDieValue;


            for (int i=0; i<Player.getPlayer(currentTestPlayer).getDice().size(); i++) {
               Die currentDie = Player.getPlayer(currentTestPlayer).getDice().get(i);
               if (currentDie.getDieValue() == actionDieValue) {
                  currentDie.markUsed();
               }
            }

         }

         if (dieValue >= defValue) {
            diceDisk[diskPos].removeCard(opponentID);
         } else {
         }
      }

   }


   @Override
   public CardActivator getActivator(int disc) {
      CardActivator activator = new MyCenturioActivator(disc);
      return activator;
   }
}