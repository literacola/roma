package Cards;

import java.io.IOException;
import java.util.Random;

import framework.interfaces.activators.CardActivator;

import CardImplimentation.MyPraetorianusActivator;
import Game.Board;
import Game.Config;
import Game.Disk;
import Game.Player;
import Game.Roma;
import Game.UserInput;

public class Praetorianus extends Card{

	private Board board;

	public Praetorianus(Board board) {
		//name, description, cost, defense, type
		super("Praetorianus", "Blocks opposing dice disk",4,4,Config.CHARACTER,framework.cards.Card.PRAETORIANUS);
		this.board = board;
	}


	public void activate() throws IOException {

		int choice = UserInput.intUserInput("Select dice disk to block from 1 to " + Config.NUM_DICEDISKS, 0, Config.NUM_DICEDISKS-1);
		
		board.getDiceDisks()[choice-1].setBlocked(Player.getOpponent());
		System.out.println("Dice disk " + choice + " is blocked");

		System.out.println("You just played a Praetorianus");

	}
	
	public void activate(int disc) throws IOException {

      int choice = disc;
      
      board.getDiceDisks()[choice-1].setBlocked(Player.getOpposingTestPlayer());
   }

	@Override
	public CardActivator getActivator(int disc) {
	   CardActivator activator = new MyPraetorianusActivator(disc);
	   return activator;
	}
}
