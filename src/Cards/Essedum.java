package Cards;

import java.io.IOException;

import framework.interfaces.activators.CardActivator;

import CardImplimentation.MyEssedumActivator;
import Game.Board;
import Game.Config;
import Game.Disk;
import Game.Player;
import Game.UserInput;

public class Essedum extends Card {

   Board board;

   public Essedum(Board board) {
      //name, description, cost, defense, type
      super("Essedum", "Reduce opponents face up card defence by 2",6,3,Config.CHARACTER,framework.cards.Card.ESSEDUM);
      this.board = board;
   }

   public void activate() throws IOException{

      Disk[] diceDisk = board.getDiceDisks();
      int[] pos = super.getPosition(this, diceDisk);

      int opponent = Player.getOpponent();

      for(int i = 0; i<Config.NUM_DICEDISKS; i++){

         if (diceDisk[i].getCard(opponent).getEnum() != framework.cards.Card.NOT_A_CARD){
            diceDisk[i].getCard(opponent).decrementDefense(Config.ESSEDUM_DECREMENT);
            System.out.println(diceDisk[i].getCard(opponent).getName() + " defense is now " + diceDisk[i].getCard(opponent).getDefense() );
            diceDisk[i].getCard(opponent).setDefenseModified();
         }
      }

      System.out.println("You just played an Essedum");

   }

   public void activate(int position) throws IOException{

      Disk[] diceDisk = board.getDiceDisks();
      int opponent = Player.getOpposingTestPlayer();

      for(int i = 0; i<Config.NUM_DICEDISKS; i++){

         if (!diceDisk[i].getCard(opponent).getEnum().equals(framework.cards.Card.NOT_A_CARD)){
            diceDisk[i].getCard(opponent).decrementDefense(Config.ESSEDUM_DECREMENT);
            diceDisk[i].getCard(opponent).setDefenseModified();
         }
      }

   }

   @Override
   public CardActivator getActivator(int disc) {
      CardActivator activator = new MyEssedumActivator(disc);
      return activator;
   }
}
