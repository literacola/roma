package Cards;

import framework.interfaces.activators.CardActivator;
import Game.Board;
import Game.Config;

public class GrimReaper extends Card {

	private Board board;

	public GrimReaper(Board board) {
		super("Grim Reaper", "Provides a chance to cheat death.", 6, 3, Config.CHARACTER, framework.cards.Card.GRIMREAPER);
		this.board = board;
	}

	@Override
	public CardActivator getActivator(int disc) {
		return null;
	}

}
