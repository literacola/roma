package Cards;

import CardImplimentation.MyTelephoneBoxActivator;
import Game.Board;
import Game.Config;
import framework.interfaces.activators.CardActivator;

public class TelephoneBox extends Card {

	private Board board;

	public TelephoneBox(Board board) {
		super("Telephone Box", "its a telephone box", 5, 2, Config.BUILDING, framework.cards.Card.TELEPHONEBOX);
		this.board = board;
	}

	@Override
	public CardActivator getActivator(int disc) {
		CardActivator activator = new MyTelephoneBoxActivator(disc);
		return activator;
	}

}
