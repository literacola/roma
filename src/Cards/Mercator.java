package Cards;

import java.io.IOException;

import framework.interfaces.activators.CardActivator;

import CardImplimentation.MyMercatorActivator;
import Game.Board;
import Game.Config;
import Game.Player;
import Game.UserInput;


public class Mercator extends Card{



   public Mercator(Board board) {
      //name, description, cost, defense, type
      super("Mercator", "Can buy enemy VP for 2 sestertii each",7,2,Config.CHARACTER,framework.cards.Card.MERCATOR);
   }


   public void activate() {

      Player currPlayer = Player.getCurrentPlayer();
      int opponentID = Player.getOpponent();
      Player opponent = Player.getPlayer(opponentID);
      int opponentVP = Player.getPlayer(opponentID).getVictoryPoints();

      System.out.println("The opponent have " + opponentVP + "VP");

      int vpToBuy = 0;

      try {
         vpToBuy = UserInput.intUserInput("how many VP do you want to buy?");
      } catch (IOException e) {
         System.out.println("bad input of VP");
      }

      int cost = vpToBuy*2;

      if((vpToBuy <= opponentVP) && (currPlayer.getSestertii() >= cost)) {
         currPlayer.addVictoryPoints(vpToBuy);
         opponent.subtractVictoryPoints(vpToBuy);

         currPlayer.subtractSestertii(cost);
         opponent.addSestertii(cost);
      } else {
         System.out.println("bad choice");
      }

      System.out.println("You have Played Mercator!");
   }

   public void activate(int VPToBuy) {

      Player currPlayer = Player.getCurrentPlayer();
      int opponentID = Player.getOpponent();
      Player opponent = Player.getPlayer(opponentID);
      int opponentVP = Player.getPlayer(opponentID).getVictoryPoints();

      int vpToBuy = VPToBuy;

      int cost = vpToBuy*2;

      if((vpToBuy <= opponentVP) && (currPlayer.getSestertii() >= cost)) {
         currPlayer.addVictoryPoints(vpToBuy);
         opponent.subtractVictoryPoints(vpToBuy);

         currPlayer.subtractSestertii(cost);
         opponent.addSestertii(cost);
      }
   }

   @Override
   public CardActivator getActivator(int disc) {
      CardActivator activator = new MyMercatorActivator(disc);
      return activator;
   }
}
