package Cards;

import java.io.IOException;
import java.util.ArrayList;

import framework.interfaces.activators.CardActivator;

import CardImplimentation.MyGladiatorActivator;
import Game.Board;
import Game.Config;
import Game.Disk;
import Game.Player;
import Game.UserInput;

public class Gladiator extends Card{

	private Board board;

	public Gladiator(Board board) {
		//name, description, cost, defense, type
		super("Gladiator", "opponents face up card returned to opponent",6,5,Config.CHARACTER,framework.cards.Card.GLADIATOR);
		this.board = board;
	}

	public void activate() throws IOException{
		Disk[] diceDisk = board.getDiceDisks();

		Card chosenCard;

		int opponentPlayerID = Player.getOpponent();
		Player opponentPlayer = Player.getPlayer(opponentPlayerID);
		ArrayList<Disk> occupiedDisks = board.getOccupiedDisks(opponentPlayerID);

		System.out.println("You just played a Gladiator! choose opponent's card!");
		
		for(int i=0; i < occupiedDisks.size(); i++) {
			System.out.println("[" + (occupiedDisks.get(i).getDiskID()+1) + "]: " + occupiedDisks.get(i).getCard(opponentPlayerID).getName());
		}

		int diskChoice = 0;

		diskChoice = UserInput.intUserInput("which card do you want to choose ", 1,6);  //magic numbers are bad mkay
	
		
		chosenCard = diceDisk[diskChoice-1].getCard(opponentPlayerID);

		opponentPlayer.receiveCard(chosenCard);
		diceDisk[diskChoice].removeCard(opponentPlayerID);

	}
	
	public void activate(int target) throws IOException{

	   Disk[] diceDisk = board.getDiceDisks();

      Card chosenCard;

      int opponentPlayerID = Player.getOpposingTestPlayer();
      Player opponentPlayer = Player.getPlayer(opponentPlayerID);
      ArrayList<Disk> occupiedDisks = board.getOccupiedDisks(opponentPlayerID);
      
      int diskChoice = target;
      
      chosenCard = diceDisk[diskChoice-1].getCard(opponentPlayerID);

      opponentPlayer.receiveCard(chosenCard);
      diceDisk[diskChoice-1].removeCard(opponentPlayerID);

   }

	@Override
	public CardActivator getActivator(int disc) {
	   CardActivator activator = new MyGladiatorActivator(disc);
	   return activator;
	}
}
