package Cards;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import framework.interfaces.activators.CardActivator;

import CardImplimentation.MyHaruspexActivator;
import Game.Board;
import Game.Config;
import Game.Player;
import Game.UserInput;

public class Haruspex extends Card {

private Board board;
	
	public Haruspex(Board board) {
		//name, description, cost, defense, type
		super("Haruspex", "can choose from draw pile and add to hand",4,3,Config.CHARACTER,framework.cards.Card.HARUSPEX);
		this.board = board;
	}
	
	
	public void activate() throws IOException {
		
		
		ArrayList<Card> deck = (ArrayList<Card>) board.getDeck();
		
		for(int i=0; i<deck.size(); i++) {
			System.out.println("["+i+"] " + deck.get(i).getName());
		}
		
		int choice = UserInput.intUserInput("Choose a card: ");
		Card chosenCard = deck.get(choice);
		Player currPlayer = Player.getCurrentPlayer();
		currPlayer.receiveCard(chosenCard);
		board.removeFromDeck(chosenCard);		
		board.shuffleDeck();
		
		System.out.println("You just played a Haruspex");
		
	}
	
	  public void activate(int chooseCardIndex, int location) {
	      
	      
	      List<Card> deck = board.getDeck();
	      
	      int choice = chooseCardIndex;
	      Card chosenCard = deck.get(choice);
	      int playerID = Player.getTestPlayer();
	      Player currPlayer = Player.getPlayer(playerID);
	      
	      currPlayer.receiveCard(chosenCard);
	      board.removeFromDeck(chosenCard);      
	      board.shuffleDeck();
	      	      
	   }

	@Override
	public CardActivator getActivator(int disc) {
	   CardActivator activator = new MyHaruspexActivator(disc);
	   return activator;
	}
}
