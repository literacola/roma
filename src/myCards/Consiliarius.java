package myCards;

import java.io.IOException;

import Game.Board;
import Game.Disk;
import Game.Player;
import Game.UserInput;

public class Consiliarius extends Card{

	private Board board;

	public Consiliarius(Board board) {
		//name, description, cost, defense
		super("Consiliarius", "pick character cards and lay to dice disk, cover building",4,4);
		this.board = board;

	}

	public void activate(){

		Disk[] diceDisk = board.getDiceDisks();
		Player curPlayer = Player.getCurrentPlayer();
		int cardChoice = 0;
		int locationChoice = 0;
		
		curPlayer.displayCards();
		System.out.println("Which card would you like to play?");
		
		try {
			cardChoice = UserInput.intUserInput("Enter choice: ", 0, curPlayer.getAllCardsInHand().size());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(curPlayer.getCard(cardChoice).getCost() > curPlayer.getSestertii()) {
			System.out.println("Not enough money, yo.");
		} else {
			curPlayer.subtractSestertii(curPlayer.getCard(cardChoice).getCost());
			System.out.println("You now have " + curPlayer.getSestertii() + " sestertii left");
			board.printBoard(false);
			System.out.println("Where would you like to place the card?");
			try {
				locationChoice = UserInput.intUserInput("Enter choice:",1,5);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			board.playCard(curPlayer.getPlayerNum(), curPlayer.playCard(cardChoice), locationChoice);
			board.printBoard(false);
		}
		System.out.println("You just played a Consiliarius!");

	}

}
