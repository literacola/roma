package myCards;
import Game.*;


public class Sicarius extends Card {

	private Board board;
	
	public Sicarius(Board board) {
		//name, description, cost, defense
		super("Sicarius", "Discards opposing character card and this card",9,2);
		this.board = board;
	}
	
	
	public void activate() {
		
		Disk[] diceDisk = board.getDiceDisks();
		int pos[] = super.getPosition(this, diceDisk);
		System.out.println("You just played a Sicarius!");
		System.out.println("I am at: " + pos[0] + " " + pos[1]);
		

		
	}

		
}
