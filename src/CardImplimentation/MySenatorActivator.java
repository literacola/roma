package CardImplimentation;

import java.io.IOException;

import Cards.Consiliarius;
import Cards.Senator;
import Game.Board;
import Game.Disk;
import Game.Player;
import Game.testHelper;
import framework.cards.Card;
import framework.interfaces.activators.SenatorActivator;

public class MySenatorActivator implements SenatorActivator{

	private int position;
	private Board board;
	private Senator senat;
	private Cards.Card myCard;
	private Object locationChoice;

	public MySenatorActivator(int disc) {
		this.position = disc - 1;
		this.board = Player.getPlayerBoard();
		
		int currentPos = position;
		int curPlayer = Player.getTestPlayer();
		Board board = Player.getPlayerBoard();
		Disk diceDisk = board.getDiceDisks()[currentPos];

		this.senat = (Senator) diceDisk.getCard(curPlayer);
		try {
			senat.activate(currentPos);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
	}

	@Override
	public void complete() {
		
	}

	@Override
	public void layCard(Card framworkCard, int whichDiceDisc) {
		this.myCard = testHelper.convertEnumToCard(framworkCard, board);
		this.locationChoice = whichDiceDisc-1;
		senat.layCard(myCard, whichDiceDisc);
	}

}
