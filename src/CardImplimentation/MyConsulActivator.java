package CardImplimentation;
import java.io.IOException;

import Game.Board;
import Game.Disk;
import Game.Player;
import framework.interfaces.activators.*;

public class MyConsulActivator implements ConsulActivator {

	private int position;
	private int changeAmt;
	private int actionDieValue;

	public MyConsulActivator(int disc) {
		this.position = disc-1;
	}

	@Override
	public void complete() {

		int currentPos = position;
		int curPlayer = Player.getTestPlayer();
		Board board = Player.getPlayerBoard();
		Disk diceDisk = board.getDiceDisks()[currentPos];
		diceDisk.getCard(curPlayer).activate(actionDieValue,changeAmt);
	}

	@Override
	public void chooseConsulChangeAmount(int amount) {
		this.changeAmt = amount;

	}

	@Override
	public void chooseWhichDiceChanges(int originalRoll) {
		this.actionDieValue = originalRoll;

	}





}
