package CardImplimentation;

import java.io.IOException;

import Game.Board;
import Game.Disk;
import Game.Player;
import framework.interfaces.activators.LegionariusActivator;

public class MyLegionariusActivator implements LegionariusActivator{

	private int position;
	private int battleDieValue;

	public MyLegionariusActivator (int disk) {
		this.position = disk-1;
	}
	@Override
	public void giveAttackDieRoll(int roll) {
		this.battleDieValue = roll;
	}

	@Override
	public void complete() {
		
		int currentPos = position;
	      int curPlayer = Player.getTestPlayer();
	      Board board = Player.getPlayerBoard();
	      Disk diceDisk = board.getDiceDisks()[currentPos];
	      try {
	         diceDisk.getCard(curPlayer).activate(battleDieValue);
	      } catch (IOException e) {
	         // TODO Auto-generated catch block
	         e.printStackTrace();
	      }		
	}

}
