package CardImplimentation;

import java.io.IOException;

import Game.Board;
import Game.Disk;
import Game.Player;
import framework.interfaces.activators.NeroActivator;

public class MyNeroActivator implements NeroActivator {

	private int position;
	private int attack;
	
	public MyNeroActivator(int disk) {
		
		this.position = disk-1;
		
	}
	
	@Override
	public void chooseDiceDisc(int diceDisc) {

		this.attack=diceDisc;
		
	}

	@Override
	public void complete() {

		int currentPos = position;
	      int curPlayer = Player.getTestPlayer();
	      Board board = Player.getPlayerBoard();
	      Disk diceDisk = board.getDiceDisks()[currentPos];
	      try {
	         diceDisk.getCard(curPlayer).activate(attack);
	      } catch (IOException e) {
	         // TODO Auto-generated catch block
	         e.printStackTrace();
	      }		
	}

		
	}
