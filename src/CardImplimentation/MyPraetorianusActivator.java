package CardImplimentation;

import java.io.IOException;

import Game.Board;
import Game.Disk;
import Game.Player;
import framework.interfaces.activators.PraetorianusActivator;

public class MyPraetorianusActivator implements PraetorianusActivator{

   private int position;
   private int target;

   public MyPraetorianusActivator(int disc) {
      this.position = disc-1;
   }

   @Override
   public void chooseDiceDisc(int diceDisc) {
      this.target = diceDisc;      
   }

   @Override
   public void complete() {
      int currentPos = position;
      int curPlayer = Player.getTestPlayer();
      Board board = Player.getPlayerBoard();
      Disk diceDisk = board.getDiceDisks()[currentPos];
      try {
         diceDisk.getCard(curPlayer).activate(target);
      } catch (IOException e) {
         // TODO Auto-generated catch block
         e.printStackTrace();
      }      
   }

}
