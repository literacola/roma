package CardImplimentation;

import framework.cards.Card;
import framework.interfaces.activators.*;

public class MyBlockedActivator implements AesculapinumActivator,ArchitectusActivator,
CenturioActivator,ConsulActivator,EssedumActivator,ForumActivator,GladiatorActivator,
HaruspexActivator,LegatActivator,LegionariusActivator,MachinaActivator,MercatorActivator,
MercatusActivator,NeroActivator,OnagerActivator,PraetorianusActivator,ScaenicusActivator,
SenatorActivator,SicariusActivator,TelephoneBoxActivator,TribunusPlebisActivator,
VelitesActivator {

   @Override
   public void complete() {


   }

   @Override
   public void chooseCardFromPile(int indexOfCard) {


   }

   @Override
   public void giveAttackDieRoll(int roll) {


   }

   @Override
   public void chooseActionDice(int actionDiceValue) {


   }

   @Override
   public void chooseDiceDisc(int diceDisc) {


   }

   @Override
   public void chooseActivateTemplum(boolean activate) {


   }

   @Override
   public void chooseActivateTemplum(int diceValue) {


   }

   @Override
   public void chooseConsulChangeAmount(int amount) {


   }

   @Override
   public void chooseWhichDiceChanges(int originalRoll) {


   }

   @Override
   public void chooseCenturioAddActionDie(boolean attackAgain) {


   }

   @Override
   public void layCard(Card myCard, int whichDiceDisc) {


   }

   @Override
   public void placeCard(Card card, int diceDisc) {


   }

   @Override
   public void shouldMoveForwardInTime(boolean isForward) {


   }

   @Override
   public void setSecondDiceUsed(int diceValue) {


   }

   @Override
   public CardActivator getScaenicusMimicTarget(int diceDisc) {

      return null;
   }

   @Override
   public void chooseMercatorBuyNum(int VPToBuy) {


   }

}
