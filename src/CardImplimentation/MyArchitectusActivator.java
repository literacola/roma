package CardImplimentation;

import java.io.IOException;

import Cards.Architectus;
import Cards.Consiliarius;
import Cards.Senator;
import Game.Board;
import Game.Disk;
import Game.Player;
import Game.testHelper;
import framework.cards.Card;
import framework.interfaces.activators.ArchitectusActivator;


public class MyArchitectusActivator implements ArchitectusActivator {

	private int position;
	private Board board;
	private Architectus archi;
	private Cards.Card myCard;
	private Object locationChoice;

	public MyArchitectusActivator(int disc) {
		this.position = disc - 1;
		this.board = Player.getPlayerBoard();
		
		int currentPos = position;
		int curPlayer = Player.getTestPlayer();
		Board board = Player.getPlayerBoard();
		Disk diceDisk = board.getDiceDisks()[currentPos];

		this.archi = (Architectus) diceDisk.getCard(curPlayer);
		try {
			archi.activate(currentPos);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
	}

	@Override
	public void complete() {
		
	}

	@Override
	public void layCard(Card framworkCard, int whichDiceDisc) {
		this.myCard = testHelper.convertEnumToCard(framworkCard, board);
		this.locationChoice = whichDiceDisc-1;
		archi.layCard(myCard, whichDiceDisc);
	}

}
