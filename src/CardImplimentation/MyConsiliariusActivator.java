package CardImplimentation;

import java.io.IOException;

import framework.cards.Card;
import framework.interfaces.activators.ConsiliariusActivator;

import Cards.Consiliarius;
import Game.Board;
import Game.Disk;
import Game.Player;
import Game.testHelper;

public class MyConsiliariusActivator implements ConsiliariusActivator {


	private int position;
	private Board board;
	private Cards.Card myCard;
	private int locationChoice;
	Consiliarius consil;
	
	public MyConsiliariusActivator (int disc) {
		this.position = disc-1;
		this.board = Player.getPlayerBoard();
		
		int currentPos = position;
		int curPlayer = Player.getTestPlayer();
		Board board = Player.getPlayerBoard();
		Disk diceDisk = board.getDiceDisks()[currentPos];

		this.consil = (Consiliarius) diceDisk.getCard(curPlayer);
		try {
			consil.activate(currentPos);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
	}



	public void complete() {

		consil.returnOtherCards();

	}



	@Override
	public void placeCard(Card card, int diceDisc) {
		this.myCard = testHelper.convertEnumToCard(card, board);
		this.locationChoice = diceDisc-1;
		consil.placeCard(myCard, diceDisc);
	}

}
