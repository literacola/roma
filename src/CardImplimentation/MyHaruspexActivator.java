package CardImplimentation;

import Game.Board;
import Game.Disk;
import Game.Player;
import framework.interfaces.activators.HaruspexActivator;

public class MyHaruspexActivator implements HaruspexActivator {

   private int position;
   private int chooseCardIndex;

   public MyHaruspexActivator(int disc) {
      this.position = disc-1;
   }


   @Override
   public void chooseCardFromPile(int indexOfCard) {
      this.chooseCardIndex = indexOfCard;
   }

   @Override
   public void complete() {
      int currentPos = position;
      int curPlayer = Player.getTestPlayer();
      Board board = Player.getPlayerBoard();
      Disk diceDisk = board.getDiceDisks()[currentPos];
      diceDisk.getCard(curPlayer).activate(chooseCardIndex, position);
      
   }

}
