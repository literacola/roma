package CardImplimentation;

import java.io.IOException;

import Game.Board;
import Game.Disk;
import Game.Player;
import framework.interfaces.activators.MercatorActivator;

public class MyMercatorActivator implements MercatorActivator {

   private int position;
   private int vpToBuy;

   public MyMercatorActivator(int disc) {
      this.position = disc-1;
   }

   @Override
   public void complete() {
      int currentPos = position;
      int curPlayer = Player.getTestPlayer();
      Board board = Player.getPlayerBoard();
      Disk diceDisk = board.getDiceDisks()[currentPos];
      try {
         diceDisk.getCard(curPlayer).activate(vpToBuy);
      } catch (IOException e) {
         // TODO Auto-generated catch block
         e.printStackTrace();
      }      
   }

   @Override
   public void chooseMercatorBuyNum(int VPToBuy) {
      this.vpToBuy = VPToBuy;
   }

}
