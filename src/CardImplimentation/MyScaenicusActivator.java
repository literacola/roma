package CardImplimentation;

import Cards.Card;
import Game.Player;
import framework.interfaces.activators.CardActivator;
import framework.interfaces.activators.ScaenicusActivator;

public class MyScaenicusActivator implements ScaenicusActivator{

	private int position;
	private int target;
	
	public MyScaenicusActivator(int disc) {
		this.position = disc-1;
	}

	@Override
	public void complete() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public CardActivator getScaenicusMimicTarget(int diceDisc) {
		return Player.getPlayerBoard().getDiceDisks()[diceDisc-1].getCard(Player.getTestPlayer()).getActivator(diceDisc);
	}

}
