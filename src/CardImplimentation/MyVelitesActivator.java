package CardImplimentation;

import java.io.IOException;

import Game.Board;
import Game.Disk;
import Game.Player;
import framework.interfaces.activators.VelitesActivator;

public class MyVelitesActivator implements VelitesActivator{

	private int position;
	private int target;
	private int battleDieValue;

	public MyVelitesActivator (int disc) {
		this.position = disc-1;
	}
	@Override
	public void chooseDiceDisc(int diceDisc) {
		this.target = diceDisc;		
	}

	@Override
	public void giveAttackDieRoll(int roll) {
		this.battleDieValue = roll;
	}

	@Override
	public void complete() {
		int currentPos = position;
	      int curPlayer = Player.getTestPlayer();
	      Board board = Player.getPlayerBoard();
	      Disk diceDisk = board.getDiceDisks()[currentPos];

	      diceDisk.getCard(curPlayer).activate(target,battleDieValue);
	     
	}

}
