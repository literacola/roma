package CardImplimentation;

import java.io.IOException;

import Game.Board;
import Game.Disk;
import Game.Player;
import framework.interfaces.activators.TribunusPlebisActivator;

public class MyTribunusPlebisActivator implements TribunusPlebisActivator{

	private int position;
	
	public MyTribunusPlebisActivator (int disc) {
		this.position = disc-1;
	}
	@Override
	public void complete() {
		int currentPos = position;
		int curPlayer = Player.getTestPlayer();
		Board board = Player.getPlayerBoard();
		Disk diceDisk = board.getDiceDisks()[currentPos];
		try {
			diceDisk.getCard(curPlayer).activate();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}

}
