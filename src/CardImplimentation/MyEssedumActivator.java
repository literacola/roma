package CardImplimentation;

import java.io.IOException;

import Game.Board;
import Game.Disk;
import Game.Player;
import framework.interfaces.activators.EssedumActivator;

public class MyEssedumActivator implements EssedumActivator {

   private int position;

   public MyEssedumActivator(int disc) {
      this.position = disc-1;
   }

   @Override
   public void complete() {
      int currentPos = position;
      int curPlayer = Player.getTestPlayer();
      Board board = Player.getPlayerBoard();
      Disk diceDisk = board.getDiceDisks()[currentPos];
      try {
         diceDisk.getCard(curPlayer).activate(position);
      } catch (IOException e) {
         // TODO Auto-generated catch block
         e.printStackTrace();
      }
   }

}
