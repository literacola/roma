package CardImplimentation;

import java.io.IOException;

import Game.Board;
import Game.Disk;
import Game.Player;
import framework.interfaces.activators.CenturioActivator;

public class MyCenturioActivator implements CenturioActivator{

   private int position;
   private int attackValue;
   private int actionDieValue;
   private boolean repeatAttack;

   public MyCenturioActivator (int disc) {
      this.position = disc-1;
   }

   @Override
   public void giveAttackDieRoll(int roll) {
      this.attackValue = roll;      
   }

   @Override
   public void chooseActionDice(int actionDiceValue) {
      this.actionDieValue = actionDiceValue;      
   }

   @Override
   public void complete() {
      int currentPos = position;
      int curPlayer = Player.getTestPlayer();
      Board board = Player.getPlayerBoard();
      Disk diceDisk = board.getDiceDisks()[currentPos];

      diceDisk.getCard(curPlayer).activate(attackValue,repeatAttack,actionDieValue);


   }

   @Override
   public void chooseCenturioAddActionDie(boolean attackAgain) {
      this.repeatAttack = attackAgain;      
   }

}
