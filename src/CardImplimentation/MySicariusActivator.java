package CardImplimentation;

import java.io.IOException;

import Game.*;
import framework.interfaces.activators.SicariusActivator;

public class MySicariusActivator implements SicariusActivator {

   private int target;
   private int position;
   
   public MySicariusActivator (int disc) {
      this.target = -1;
      this.position = disc-1;
   }
   @Override
   public void chooseDiceDisc(int diceDisc) {
      target = diceDisc;
   }

   @Override
   public void complete() {
      int currentPos = position;
      int curPlayer = Player.getTestPlayer();
      Board board = Player.getPlayerBoard();
      Disk diceDisk = board.getDiceDisks()[currentPos];
      diceDisk.getCard(curPlayer).activate(target, position);
     
   }
   
}
