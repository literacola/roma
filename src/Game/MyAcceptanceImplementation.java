package Game;

import framework.interfaces.GameState;
import framework.interfaces.MoveMaker;

public class MyAcceptanceImplementation implements framework.interfaces.AcceptanceInterface{

	private MyMoveMakerImplementation maker;
	private MyGameStateImplementation state;
	
	@Override
	public MoveMaker getMover(GameState state) {
		this.maker = new MyMoveMakerImplementation(state); 
		return this.maker;
	}

	@Override
	public GameState getInitialState() {
		this.state = new MyGameStateImplementation();
		return this.state;
	}

}
