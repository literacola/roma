package Game;
import Cards.*;
import framework.cards.*;

public class testHelper {

	public static Cards.Card convertEnumToCard(framework.cards.Card card, Board board) {  //this sucks donkeyballs

		Cards.Card chosenOne = null;
		if(card == framework.cards.Card.ARCHITECTUS) {
			chosenOne = new Architectus(board);
		} else if(card == framework.cards.Card.CENTURIO) {
			chosenOne = new Centurio(board);
		} else if(card == framework.cards.Card.CONSILIARIUS) {
			chosenOne = new Consiliarius(board);
		} else if(card == framework.cards.Card.CONSUL) {
			chosenOne = new Consul(board);
		} else if(card == framework.cards.Card.ESSEDUM) {
			chosenOne = new Essedum(board);
		} else if(card == framework.cards.Card.GLADIATOR) {
			chosenOne = new Cards.Gladiator(board);
		} else if(card == framework.cards.Card.HARUSPEX) {
			chosenOne = new Haruspex(board);
		} else if(card == framework.cards.Card.LEGAT) {
			chosenOne = new Legat(board);
		} else if(card == framework.cards.Card.LEGIONARIUS) {
			chosenOne = new Legionarius(board);
		} else if(card == framework.cards.Card.MERCATOR) {
			chosenOne = new Mercator(board);
		} else if(card == framework.cards.Card.NERO) {
			chosenOne = new Nero(board);
		} else if(card == framework.cards.Card.PRAETORIANUS) {
			chosenOne = new Praetorianus(board);
		} else if(card == framework.cards.Card.SCAENICUS) {
			chosenOne = new Scaenicus(board);
		} else if(card == framework.cards.Card.SENATOR) {
			chosenOne = new Senator(board);
		} else if(card == framework.cards.Card.SICARIUS) {
			chosenOne = new Sicarius(board);
		} else if(card == framework.cards.Card.TRIBUNUSPLEBIS) {
			chosenOne = new TribunusPlebis(board);
		} else if(card == framework.cards.Card.VELITES) {
			chosenOne = new Velites(board);
		} else if(card == framework.cards.Card.AESCULAPINUM) {
			chosenOne = new Aesculapinum(board);
		} else if(card == framework.cards.Card.BASILICA) {
			chosenOne = new Basilica(board);
		} else if(card == framework.cards.Card.MACHINA) {
			chosenOne = new Machina(board);
		} else if(card == framework.cards.Card.FORUM) {
			chosenOne = new Forum(board);
		} else if(card == framework.cards.Card.MERCATUS) {
			chosenOne = new Mercatus(board);
		} else if(card == framework.cards.Card.ONAGER) {
			chosenOne = new Onager(board);
		} else if(card == framework.cards.Card.TEMPLUM) {
			chosenOne = new Templum(board);
		} else if(card == framework.cards.Card.TURRIS) {
			chosenOne = new Turris(board);
		} else if(card == framework.cards.Card.KAT) {
			chosenOne = new Kat(board);
		} else if(card == framework.cards.Card.GRIMREAPER) {
			chosenOne = new GrimReaper(board);
		}  else if(card == framework.cards.Card.TELEPHONEBOX) {
			chosenOne = new TelephoneBox(board);
		} else if(card == framework.cards.Card.NOT_A_CARD) {
			chosenOne = new NotACard(board);
		} else {
			chosenOne = new NotACard(board);
		}
		return chosenOne;
	}
	
}
