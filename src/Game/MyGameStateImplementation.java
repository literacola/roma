package Game;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Stack;

import Cards.*;
import framework.cards.Card;


public class MyGameStateImplementation implements framework.interfaces.GameState {

	private Roma roma;
	//	private Player player;
	public Board board;

	public MyGameStateImplementation() {

		try {
			roma = new Roma();
			roma.initTestGame();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		board = roma.getBoard();
//		this.board = Player.getPlayerBoard();
	}

	@Override
	public int getWhoseTurn() {

//		return Player.getPlayerNum();
		return Player.getTestPlayer();
	}

	@Override
	public void setWhoseTurn(int player) {
		Player.setTestPlayer(player);

	}

	@Override
	public List<Card> getDeck() {
		ArrayList<framework.cards.Card> testDeck = new ArrayList<framework.cards.Card>();
		for(Cards.Card c : board.getDeck()) {
			testDeck.add(c.getEnum());
		}
		return testDeck;

	}

	@Override
	public void setDeck(List<Card> deck) {
		Stack<Cards.Card> drawPile = new Stack<Cards.Card>();

		for(framework.cards.Card c : deck) {
			drawPile.add(testHelper.convertEnumToCard(c, board));
		}
		board.setDrawPile(drawPile);
	}



	@Override
	public List<Card> getDiscard() {
		ArrayList<framework.cards.Card> testDeck = new ArrayList<framework.cards.Card>();
		for(Cards.Card c : board.getDiscardPile()) {
			testDeck.add(c.getEnum());
		}
		return testDeck;
	}

	@Override
	public void setDiscard(List<Card> discard) {

		Stack<Cards.Card> discardPile = new Stack<Cards.Card>();

		for(framework.cards.Card c : discard) {
			discardPile.add(testHelper.convertEnumToCard(c, board));
		}

		board.setDiscardPile(discardPile);
	}

	@Override
	public int getPlayerSestertii(int playerNum) {
		int sestertii = Player.getPlayer(playerNum).getSestertii();
		return sestertii;
	}

	@Override
	public void setPlayerSestertii(int playerNum, int amount) {
		Player.getPlayer(playerNum).setSestertti(amount);
	}

	@Override
	public int getPlayerVictoryPoints(int playerNum) {

		return Player.getPlayer(playerNum).getVictoryPoints();
	}

	@Override
	public void setPlayerVictoryPoints(int playerNum, int points) {
		Player.getPlayer(playerNum).setVP(points);

	}

	@Override
	public Collection<Card> getPlayerHand(int playerNum) {
		ArrayList<Cards.Card> hand = Player.getPlayer(playerNum).getAllCardsInHand();
		ArrayList<framework.cards.Card> playerHand = new ArrayList<framework.cards.Card>();
		
		for(Cards.Card c : hand) {
			playerHand.add(c.getEnum());
		}

		return playerHand;
	}

	@Override
	public void setPlayerHand(int playerNum, Collection<Card> hand) {
		ArrayList<Cards.Card> playerHand = new ArrayList<Cards.Card>();

		for(framework.cards.Card c : hand) {
			Cards.Card ourCard = testHelper.convertEnumToCard(c, board);
			playerHand.add(ourCard);
		}

		Player.getPlayer(playerNum).receiveNewCards(playerHand);
	}

	@Override
	public Card[] getPlayerCardsOnDiscs(int playerNum) {		
		framework.cards.Card[] playerCardsOnDisks = new framework.cards.Card[Config.NUM_DICEDISKS];
		Disk[] diceDisks = board.getDiceDisks();

		for(int i = 1; i<Config.NUM_DICEDISKS+1; i++) {
			if(diceDisks[i-1] == null) {
				playerCardsOnDisks[i-1]=framework.cards.Card.NOT_A_CARD;
			} else {
				playerCardsOnDisks[i-1]=diceDisks[i-1].getCard(playerNum).getEnum();
			}
		}

		return playerCardsOnDisks;

	}

	@Override
	public void setPlayerCardsOnDiscs(int playerNum, Card[] discCards) {

		for(int i=0; i<discCards.length; i++) {
			board.playCard(playerNum, testHelper.convertEnumToCard(discCards[i], board), i);
		}

	}

	@Override
	public int[] getActionDice() {
	   int curTestPlayer = Player.getTestPlayer();
	    return Player.getPlayer(curTestPlayer).getUnusedDiceArray();
	}

	@Override
	public void setActionDice(int[] dice) {
	   int curTestPlayer = Player.getTestPlayer();
	   Player.getPlayer(curTestPlayer).setDiceValues(dice);

	}

	@Override
	public int getPoolVictoryPoints() {
		return board.getNumVPLeft();
	}



	@Override
	public boolean isGameCompleted() {
		return roma.isGameOver();
	}

	public Board getBoard() {
		return board;
	}
	
}
