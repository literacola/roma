package Game;

import java.io.IOException;
import java.util.*;

import Cards.*;

public class Board {


   private Stack<Card> discardPile = new Stack<Card>();
   private static Disk[] diceDisks = new Disk[Config.NUM_DICEDISKS]; 
   private int VPStockPile;
   private List<Card> deck = new ArrayList<Card>();



   public Board() {
      createCards();
      createDisks();
      this.VPStockPile = Config.GAME_VICTORY_POINTS;
   }

   public void swapCards(Player A, Player B, int numCards) throws IOException {
      ArrayList<Card> playerAEscrow = new ArrayList<Card>();
      ArrayList<Card> playerBEscrow = new ArrayList<Card>();
      int choice;

      System.out.println(A.getUserName() + ", choose which cards you would like to swap: ");
      for(int i=0; i<numCards; i++) {
         for(int j=0; j<A.getAllCardsInHand().size(); j++) {
            System.out.println("[" + j + "] " + A.getCard(j).getName()); 
         }

         choice = UserInput.intUserInput("Enter card to swap: ", 0,Config.NUM_INIT_CARDS);
         playerAEscrow.add(A.getCard(choice));
         A.removeCard(choice);	
      }

      System.out.println(B.getUserName() + ", choose which cards you would like to swap: ");
      for(int i=0; i<numCards; i++) {
         for(int j=0; j<B.getAllCardsInHand().size(); j++) {
            System.out.println("[" + j + "] " + B.getCard(j).getName()); 
         }

         choice = UserInput.intUserInput("Enter card to swap: ", 0, Config.NUM_INIT_CARDS);
         playerBEscrow.add(B.getCard(choice));
         B.removeCard(choice);	
      }

      A.receiveCards(playerBEscrow);
      B.receiveCards(playerAEscrow);

      System.out.println("\n\n\n");	
   }

   public void playCard(int playerID, Card card, int diskID) {
      diceDisks[diskID].playCard(card, playerID);
      Player.getPlayer(playerID).removeCard(card);
   }


   public void printBoard(boolean hideCards) {
      System.out.println("\n\n\n");
      Card[] cardsOnDisk;

      //print Player 1 cards
      System.out.print(Player.getPlayer(0).getUserName() + ":               ");
      for(Disk d : diceDisks) {
         cardsOnDisk=d.getCardsOnDisk();

         if(cardsOnDisk[0].getEnum() != framework.cards.Card.NOT_A_CARD) {
            if(hideCards == true) {
               System.out.print("X                     ");
            } else {
               System.out.print(cardsOnDisk[0].getName()+"          ");
            }
         } else {
            System.out.print("                     ");
         }
      }		


      System.out.println("");
      System.out.print("                  ");
      //print DiceDisks
      for(Disk d : diceDisks) {
         System.out.print("   |     " + (d.getDiskID()+1) + "     |   ");
      }

      System.out.println("");
      //print Player 1 cards
      System.out.print(Player.getPlayer(1).getUserName() + ":               ");
      for(Disk d : diceDisks) {
         cardsOnDisk=d.getCardsOnDisk();

         if(cardsOnDisk[1].getEnum() != framework.cards.Card.NOT_A_CARD) {
            if(hideCards == true) {
               System.out.print("X                     ");
            } else {
               System.out.print(cardsOnDisk[1].getName()+"          ");
            }
         } else {
            System.out.print("                     ");
         }
      }


      System.out.println("\n\n\n");
   }


   public void discard(Card card) {
      discardPile.push(card);
   }



   public int getVP(int numVP) {
      int returnedVP=0;

      if(numVP > VPStockPile) {
         returnedVP=numVP - VPStockPile;
         this.VPStockPile = 0;
      } else {
         returnedVP = numVP;
         this.VPStockPile-=returnedVP;
      }

      return(returnedVP);
   }




   public int getNumVPLeft() {
      return(this.VPStockPile);
   }



   private void createCards() {


      for(int i= 0; i<Config.NUM_TRIBUNUS; i++) {
         TribunusPlebis C = new TribunusPlebis(this);	
         deck.add(C);
      }
      for(int i= 0; i<Config.NUM_SICARIUS; i++) {
         Sicarius C = new Sicarius(this);	
         deck.add(C);
      }
      for(int i= 0; i<Config.NUM_LEGAT; i++) {
         Legat C = new Legat(this);	
         deck.add(C);
      }
      for(int i= 0; i<Config.NUM_ESSEDUM; i++) {
         Essedum C = new Essedum(this);	
         deck.add(C);
      }
      for(int i= 0; i<Config.NUM_ARCHITECTUS; i++) {
         Architectus C = new Architectus(this);	
         deck.add(C);
      }
      for(int i= 0; i<Config.NUM_GLADIATOR; i++) {
         Gladiator C = new Gladiator(this);	
         deck.add(C);
      }
      for(int i= 0; i<Config.NUM_LEGIONARIUS; i++) {
         Legionarius C = new Legionarius(this);	
         deck.add(C);
      }
      for(int i= 0; i<Config.NUM_VELITES; i++) {
         Velites C = new Velites(this);	
         deck.add(C);
      }
      for(int i= 0; i<Config.NUM_HARUSPEX; i++) {
         Haruspex C = new Haruspex(this);	
         deck.add(C);
      }
      for(int i= 0; i<Config.NUM_SCAENICUS; i++) {
         Scaenicus C = new Scaenicus(this);	
         deck.add(C);
      }
      for(int i= 0; i<Config.NUM_CENTURIO; i++) {
         Centurio C = new Centurio(this);	
         deck.add(C);
      }
      for(int i= 0; i<Config.NUM_CONSUL; i++) {
         Consul C = new Consul(this);	
         deck.add(C);
      }
      for(int i= 0; i<Config.NUM_SENATOR; i++) {
         Senator C = new Senator(this);	
         deck.add(C);
      }
      for(int i= 0; i<Config.NUM_PRAETORIANUS; i++) {
         Praetorianus C = new Praetorianus(this);	
         deck.add(C);
      }
      for(int i= 0; i<Config.NUM_CONSILIARIUS; i++) {
         Consiliarius C = new Consiliarius(this);  
         deck.add(C);
      }
      for(int i= 0; i<Config.NUM_MERCATOR; i++) {
         Mercator C = new Mercator(this);  
         deck.add(C);
      }
      for(int i= 0; i<Config.NUM_NERO; i++) {
         Nero C = new Nero(this);  
         deck.add(C);
      }
      for(int i= 0; i<Config.NUM_KAT; i++) {
         Kat C = new Kat(this);  
         deck.add(C);
      }
      for(int i= 0; i<Config.NUM_GRIMREAPER; i++) {
         GrimReaper C = new GrimReaper(this);  
         deck.add(C);
      }

      //building cards
//      for(int i= 0; i<Config.NUM_AESCULAPINUM; i++) {
//         Aesculapinum C = new Aesculapinum(this); 
//         deck.add(C);
//      }
//      for(int i= 0; i<Config.NUM_BASILICA; i++) {
//         Basilica C = new Basilica(this);  
//         deck.add(C);
//      }
//      for(int i= 0; i<Config.NUM_MACHINA; i++) {
//         Machina C = new Machina(this); 
//         deck.add(C);
//      }
//      for(int i= 0; i<Config.NUM_FORUM; i++) {
//         Forum C = new Forum(this);  
//         deck.add(C);
//      }
//      for(int i= 0; i<Config.NUM_MERCATUS; i++) {
//         Mercatus C = new Mercatus(this);   
//         deck.add(C);
//      }
//      for(int i= 0; i<Config.NUM_ONAGER; i++) {
//         Onager C = new Onager(this);  
//         deck.add(C);
//      }
//      for(int i= 0; i<Config.NUM_TEMPLUM; i++) {
//         Templum C = new Templum(this);  
//         deck.add(C);
//      }
//      for(int i= 0; i<Config.NUM_TURRIS; i++) {
//         Turris C = new Turris(this);  
//         deck.add(C);
//      }
//      for(int i= 0; i<Config.NUM_TELEPHONEBOX; i++) {
//         TelephoneBox C = new TelephoneBox(this);  
//         deck.add(C);
//      }

      shuffleDeck();

   }

   private void createDisks() {

      for(int i=0; i<Config.NUM_DICEDISKS; i++) {
         diceDisks[i] = new Disk(this,i);
      }

   }


   private void printStack(Stack<Card> printMe) {

      System.out.println("Printing stack:");
      System.out.println("------------------------------------------------------------------------");
      for(Card c : printMe) {
         System.out.println(c.getName());
      }
      System.out.println("------------------------------------------------------------------------");

   }


   public ArrayList<Card> drawCards(int num) {
      ArrayList<Card> drawnCards = new ArrayList<Card>();
      int numCardsDrawn=0;
      int originalDeckSize = deck.size();
      System.out.println("deck size is " + deck.size());

      while(numCardsDrawn < num && !deck.isEmpty()) {
         drawnCards.add(deck.remove(0));
         numCardsDrawn++;
      }


      //use discard pile as new deck
      if(deck.size() == 0) {
         Stack<Card> newPile = (Stack<Card>) discardPile.clone();
         this.setDrawPile(newPile);
         discardPile.clear();
         if(numCardsDrawn < num) {
            for(int i=0; i<num-numCardsDrawn; i++) {
               drawnCards.add(deck.remove(0));
            }
         }

      }

      System.out.println("drawn " + numCardsDrawn + " cards");
      return(drawnCards);

   }

   public void removeFromDeck(Card c) {
      deck.remove(c);
   }

   public void layCardsFaceDown(int numCards, int playerID) throws IOException {
      int diskChoice;
      Player player = Player.getPlayer(playerID);

      System.out.println("Player " + (playerID+1) + ", please place your cards face down");


      for(int i=0; i<numCards; i++) {
         printBoard(true);
         diskChoice = UserInput.intUserInput("Please select a location for card " + (i+1) + ":",1, Config.NUM_DICEDISKS);
         diskChoice-=1;  //since disks start with 0
         playCard(playerID, player.getCard(0), diskChoice);
      }
   }


   public Disk[] getDiceDisks() {
      return(diceDisks);
   }

   public int getNumUnoccupiedDisks(int playerID) {
      int numUnoccupied=0;

      Disk[] disks = getDiceDisks();

      for(Disk d : disks) {
         numUnoccupied+=d.getNumUnoccupied(playerID);
      }
      return(numUnoccupied);
   }

   public int getNumUnoccupiedAndUnblockedDisks(int playerID) {
      int numUnoccupied=0;

      Disk[] disks = getDiceDisks();

      for(Disk d : disks) {
         numUnoccupied+=d.getNumUnoccupiedAndUnblocked(playerID);
      }
      return(numUnoccupied);
   }

   public List<Card> getDeck() {
      return deck;
   }

   public Stack<Card> getDiscardPile() {
      return discardPile;
   }

   public List<framework.cards.Card> getTestDeck() {

      ArrayList<framework.cards.Card> testDeck = new ArrayList<framework.cards.Card>();

      for(Card c : this.deck) {
         testDeck.add(c.getEnum());
      }

      return testDeck;


   }

   public ArrayList<Disk> getUnoccupiedDisks(int playerID) {
      ArrayList<Disk> unOcDisks = new ArrayList<Disk>();

      for(Disk d: diceDisks) {
         if(!d.isOccupied(playerID)) {
            unOcDisks.add(d);
         }
      }

      return(unOcDisks);

   }

   public ArrayList<Disk> getOccupiedDisks(int playerID) {
      ArrayList<Disk> ocDisks = new ArrayList<Disk>();

      for(Disk d: diceDisks) {
         if(d.isOccupied(playerID)) {
            ocDisks.add(d);
         }
      }

      return(ocDisks);

   }


   public void shuffleDeck() {
      Collections.shuffle(deck);	
   }


   public void setDiscardPile(Stack<Cards.Card> discardPile) {

      this.discardPile = discardPile;

   }

   public void subtractVP(int num) {
      this.VPStockPile -= num;
   }

   public void addVP(int num) {
      this.VPStockPile += num;
   }

   public void setDrawPile(Stack<Cards.Card> drawPile) {
      this.deck = drawPile;		
   }

   public void setBoardVP(int points) {
      //		this.VPStockPile = Config.INIT_VP-points;
      this.VPStockPile = points;
   }

   public boolean isGrimReaperInPlay(int playerID) {
      boolean result = false;
      for(Disk d : diceDisks) {
         if(d.getCard(playerID).getEnum() == framework.cards.Card.GRIMREAPER) {
            result = true;
         }
      }
      return result;
   }




}
