package Game;

import java.util.ArrayList;

import Cards.*;

public class Disk {

	private Card[] cardsOnDisk = new Card[Config.NUM_PLAYERS];
	private Board board;
	private int diskID;
	private boolean blocked[] = new boolean[Config.NUM_PLAYERS];

	Disk(Board board, int diskID) {
		this.board = board;
		this.diskID = diskID;
		for(int i=0; i<Config.NUM_PLAYERS; i++) {
			blocked[i] = false;
			cardsOnDisk[i] = new NotACard(board);
		}

	}

	public void playCard(Card card, int playerID) {
		if(!cardsOnDisk[playerID].getEnum().equals(framework.cards.Card.NOT_A_CARD)) {
			board.discard(cardsOnDisk[playerID]);
		} 
		cardsOnDisk[playerID] = card;

	}

	public void playCard(Card card, int playerID, boolean isUsingSenator) {
		cardsOnDisk[playerID] = card;	
	}


	public Card getCard(int playerID) {

		Card retVal = cardsOnDisk[playerID];
		if (retVal == null) {
			retVal = new NotACard(board);
		}
		return retVal;

	}

	public Card[] getCardsOnDisk() {
		return(this.cardsOnDisk);
	}

	public boolean isOccupied(int playerID) {
		boolean result=false;

		if(!cardsOnDisk[playerID].getEnum().equals(framework.cards.Card.NOT_A_CARD)) {
			result=true;
		}

		return(result);
	}

	public boolean isOccupiedAtAll() {
		boolean result=false;

		for(int i=0; i<Config.NUM_PLAYERS; i++) {
			if(cardsOnDisk[i].getEnum() != framework.cards.Card.NOT_A_CARD) {
				result=true;
			}
		}
		return(result);
	}

	public int getNumUnoccupied(int playerID) {
		int numUnoccupied=0;

		if(cardsOnDisk[playerID].getEnum() == framework.cards.Card.NOT_A_CARD) {
			numUnoccupied++;
		}

		return(numUnoccupied);
	}
	
	public int getNumUnoccupiedAndUnblocked (int playerID) {
      int numUnoccupied=0;

      if((!blocked[1 ^ playerID]) && (cardsOnDisk[playerID].getEnum() == framework.cards.Card.NOT_A_CARD)) {
         numUnoccupied++;
      }

      return(numUnoccupied);
   }

	public void printDisk() {

		System.out.print("Disk:" + this.getDiskID());

	}


	public void printCardsOnDisk() {
		System.out.println("Cards on disk:");
		for(int i=0; i<Config.NUM_PLAYERS; i++){

			if(this.cardsOnDisk[i].getEnum() != framework.cards.Card.NOT_A_CARD) {
				System.out.println("Player " + i + ": " + cardsOnDisk[i].getName());
			}
		}	
	}

	public int getDiskID() {
		return(this.diskID);
	}

	public void removeCard(int playerNum) {


		if(board.isGrimReaperInPlay(playerNum)) {
		   System.out.println("Grim Reaper is In Play");
			if(cardsOnDisk[playerNum].getEnum() == framework.cards.Card.KAT) {
				Kat kat = (Cards.Kat) cardsOnDisk[playerNum];
				System.out.println("Remaining Kat lives =  " + kat.getLives());
				if(kat.getLives() == 1) {
					Player.getPlayer(playerNum).receiveCard(cardsOnDisk[playerNum]);
					cardsOnDisk[playerNum] = new NotACard(board);
				} else {
					kat.killOnce();
				}
			} else {
				Player.getPlayer(playerNum).receiveCard(cardsOnDisk[playerNum]);
				cardsOnDisk[playerNum] = new NotACard(board);
			}

		} else {


			if(cardsOnDisk[playerNum].getEnum() == framework.cards.Card.KAT) {
				Kat kat = (Cards.Kat) cardsOnDisk[playerNum];
				System.out.println("Remaining Kat lives =  " + kat.getLives());
				if(kat.getLives() == 1) {
					board.discard(cardsOnDisk[playerNum]);
					cardsOnDisk[playerNum] = new NotACard(board);
				} else {
					kat.killOnce();
				}
			} else {
				board.discard(cardsOnDisk[playerNum]);
				cardsOnDisk[playerNum] = new NotACard(board);
			}


		}

	}
	
	public void removeCardByNero (int playerNum) {
	   board.discard(cardsOnDisk[playerNum]);
      cardsOnDisk[playerNum] = new NotACard(board);
	}

	public boolean isBlocked(int playerID) {
		return this.blocked[playerID];
	}

	public void setBlocked(int playerID) {
		this.blocked[playerID] = true;
	}

	public void resetBlocked(int playerID) {
		this.blocked[playerID] = false;
	}

	public Card pickUpCard(int playerID) {
		Card card = this.cardsOnDisk[playerID]; 
		this.cardsOnDisk[playerID] = new NotACard(board);
		return card;
	}

}