package Game;

//import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
//import java.util.Iterator;
//import java.util.Scanner;

import Cards.Card;
import static Game.Config.*;

public class Roma {

	private boolean running;
	private static int turn;
	Screen screen;
	UserInput userIn;
	public Board board;
	//	private RomaGameState state;
	private static boolean praetorianusActive;
	private static int praetorianusTurn;
	private static boolean essedumActive;
	private static int essedumTurn;

	public Roma() throws IOException {
		userIn = new UserInput(); 
		board = new Board();
	}

	public Roma(String file) throws IOException {
		userIn = new UserInput(file, this); 
	}

	public void initializeGame() throws IOException {
		running = true;
		//		state = new RomaGameState();
		printOpeningMessages();
		Player.initializePlayers(board, userIn);
		board.swapCards(Player.getPlayer(PLAYER1), Player.getPlayer(PLAYER2), NUM_CARDS_INIT_SWAP);  //brittle
		board.layCardsFaceDown(NUM_FACE_DOWN_CARDS, PLAYER1);
		board.layCardsFaceDown(NUM_FACE_DOWN_CARDS, PLAYER2);
		board.printBoard(false);
		turn=0;
		

	}


	public void runGame() throws IOException {
		initializeGame();
		Player currentPlayer;

		while(running) {
			//			if(isEssedumActive() && turn == getEssedumTurn()+2){
			//				
			//			}
			currentPlayer=Player.getCurrentPlayer();
			promptUser();
			doPhase1(currentPlayer);
			doPhase2(currentPlayer);
			doPhase3();
			resetBlockedStatus();
			resetEssedum();
			if(isGameOver()) {
				printGameOverMessage();
				this.stopGame();
			} else {
				
				nextTurn();
			}
		}
	}



	public void promptUser() {
		System.out.println("\n\n\n--------------------------------" + Player.getCurrentPlayer().getUserName() + ", you're up!--------------------------------\n\n\n");
	}




	public void doPhase1(Player currentPlayer)  {
		int numUnoccupiedDisks=0;
		System.out.println("Phase 1");
		numUnoccupiedDisks = board.getNumUnoccupiedDisks(currentPlayer.getPlayerNum());
		System.out.println(currentPlayer.getUserName() + " lost " + numUnoccupiedDisks + " victory points");
		currentPlayer.subtractVictoryPoints(numUnoccupiedDisks);
		System.out.println(currentPlayer.getUserName() + " has " + currentPlayer.getVictoryPoints() + " victory points");
		System.out.println("\n");

	}



	public void doPhase2(Player currentPlayer) {
		System.out.println("Phase 2");
		currentPlayer.rollActionDice();
		currentPlayer.displayActionDiceValues();
		System.out.println("\n");
	}



	public boolean isGameOver() {
		boolean result=false;
		int[] playerVP = Player.getAllPlayerVP();

		for(int i=0; i<playerVP.length; i++) {
			if(playerVP[i] == 0) {
				result=true;
			}
		}

		if(board.getNumVPLeft() == 0) {
			result=true;
		}

		return(result);
	}



	public void phase3ActivateCard(Player curPlayer, int disk, int diceCount) throws IOException {
		Disk disks[] = board.getDiceDisks();

		if(!disks[disk-1].isBlocked(curPlayer.getPlayerNum())) {
		//-1 since disks starts from zero
		disks[disk-1].getCard(curPlayer.getPlayerNum()).activate();
		} else {
			System.out.println("Dice disk is blocked");
			curPlayer.getDice().get(diceCount).resetDie();
		}

	}





	public void phase3DrawCards(Player curPlayer, int numCards) throws IOException {
		ArrayList<Card> tmpCards = new ArrayList<Card>();
		int cardSelect;

		tmpCards = board.drawCards(numCards);
		System.out.println("You drew: ");
		for(int i=0; i<tmpCards.size(); i++) {
			System.out.println("[" + i + "] " + tmpCards.get(i).getName());
		}
		System.out.println("Which card would you like to take?");
		cardSelect = UserInput.intUserInput("Enter selection: ", 0, tmpCards.size());
		curPlayer.receiveCard(tmpCards.get(cardSelect));
		tmpCards.remove(tmpCards.get(cardSelect));
		for(Card c: tmpCards) {
			board.discard(c);
		}
		curPlayer.displayCards();

	}



	public void doPhase3() throws IOException {

		Player curPlayer = Player.getCurrentPlayer();
		int diceCount = 0;
		int d;
		int cardChoice;
		int locationChoice;


		System.out.println("Phase 3");

		System.out.print("You currently hold ");
		if(curPlayer.getAllCardsInHand().size() == 0) {
			System.out.println(" no cards in your hand.\n");
		} else {
			System.out.println("the following cards in your hand:");
			curPlayer.displayCards();
			System.out.println("\n");
		}


		while((diceCount < Config.NUM_ACTION_DICE) && (!curPlayer.getUnusedDice().isEmpty())) {

			if (curPlayer.getDice().get(diceCount).isUsed()) {
				diceCount++;
			}

			//			d = curPlayer.getDiceValues()[diceCount]; //this wont be changed by cards.
			d = curPlayer.getDice().get(diceCount).getDieValue();

			System.out.println("What would you like to do with the " + d + " you rolled?");
			System.out.println("You can either [1] Lay Cards, [2] Take Cards, [3] Activate a card, [4] Take money, [5] Print board, [6] See wutchoo GOT, [7] Activate bribe disk");
			String choice = UserInput.staticUserInput("Enter your choice: ");




			if(choice.equals("1")){ // Lay Cards
				if(Player.getCurrentPlayer().getAllCardsInHand().isEmpty()) {
					System.out.println("You've no cards to lay, dumbass");
				} else {
					curPlayer.displayCards();
					System.out.println("Which card would you like to play?");
					cardChoice = UserInput.intUserInput("Enter choice: ", 0, curPlayer.getAllCardsInHand().size());
					if(curPlayer.getCard(cardChoice).getCost() > curPlayer.getSestertii()) {
						System.out.println("Not enough money, yo.");
					} else {
						curPlayer.subtractSestertii(curPlayer.getCard(cardChoice).getCost());
						System.out.println("You now have " + curPlayer.getSestertii() + " sestertii left");
						board.printBoard(false);
						System.out.println("Where would you like to place the card?");
						locationChoice = UserInput.intUserInput("Enter choice:",1,Config.NUM_DICEDISKS);
						board.playCard(curPlayer.getPlayerNum(), curPlayer.playCard(cardChoice), locationChoice-1);
						board.printBoard(false);
					}
				}

			} else if(choice.equals("2")) { //Draw Cards
				curPlayer.getDice().get(diceCount).markUsed();
				phase3DrawCards(curPlayer, d);

				diceCount++;



			} else if(choice.equals("3")) { // Activate Card
				curPlayer.getDice().get(diceCount).markUsed();
				Die currentDie = curPlayer.getDice().get(diceCount);
				curPlayer.setCurrentActionDie(currentDie);
				curPlayer.setCurrentDiskPosition(currentDie.getDieValue());

				if(board.getDiceDisks()[d-1].isOccupied(curPlayer.getPlayerNum())) {

					phase3ActivateCard(curPlayer, d, diceCount);
					diceCount++;

				} else {
					System.out.println("Disk not occupied with a card, tough luck!");
				}


			} else if(choice.equals("4")) { //Take money
				curPlayer.getDice().get(diceCount).markUsed();

				curPlayer.addSestertii(d);
				System.out.println(curPlayer.getUserName() + " now has " + curPlayer.getSestertii() + " Sestertii");
				//RomaGameState state = new RomaGameState();
				//System.out.println(state.getPlayerSestertii(0));
				//System.out.println(state.getPlayerSestertii(state.getWhoseTurn()));
				//System.out.println(state.getPlayerVictoryPoints(state.getWhoseTurn()));
				//System.out.println(state.getPoolVictoryPoints());

				diceCount++;


			} else if(choice.equals("5")) {

				board.printBoard(false);

			} else if(choice.equals("6")) { //SEE WHATCHOO GOT

				curPlayer.barfPlayerInfo();

			} else if(choice.equals("7")) {

				if(board.getDiceDisks()[Config.BRIBE_DISK-1].isOccupied(curPlayer.getPlayerNum())) {

					System.out.println("sup from 7, d = ");

					if(curPlayer.getSestertii() >= d) {
						curPlayer.getDice().get(diceCount).markUsed();
						curPlayer.setCurrentDiskPosition(Config.BRIBE_DISK-1);
						phase3ActivateCard(curPlayer, d, diceCount);
						diceCount++;
						curPlayer.subtractSestertii(d);

					}

				}
			} else if(choice.equals("8")) {
				curPlayer.barfBoard();
			} else {
				System.out.println("You gave me something bad");
			}

			System.out.println("unused die = " + curPlayer.getUnusedDice());

		}

	}


	public static void setPraetorianusTurn(int turn) {
		praetorianusTurn = turn;	
	}

	public static int getPraetorianusTurn() {
		return(praetorianusTurn);	
	}

	public static void setEssedumActive() {
		essedumActive = true;

	}

	public static void setEssedumTurn(int currentTurn) {
		essedumTurn = currentTurn;

	}

	public static int getEssedumTurn() {
		return(essedumTurn);	
	}

	public boolean isEssedumActive() {
		return (essedumActive);
	}


	public static int getTurn() {
		return Roma.turn;
	}

	public void nextTurn() {
		turn++;
	}

	public void stopGame() {
		running = false;
	}

	public void resetBlockedStatus() {
		for(Disk d : board.getDiceDisks()) {
			d.resetBlocked(Player.getCurrentPlayer().getPlayerNum());
		}
	}
	
	public void resetEssedum() {
		for(Disk d : board.getDiceDisks()) {
			Card c = d.getCard(Player.getCurrentPlayer().getPlayerNum());
			if(c != null) { 
				c.resetDefenseModified();
			}
		}
	}
	
	public void printGameOverMessage() {

		int[] playerVP = Player.getAllPlayerVP();
		int mostVP=0;
		Player winningPlayer = null;

		for(int i=0; i<Config.NUM_PLAYERS; i++) {
			if(playerVP[i] > mostVP) {
				winningPlayer = Player.getPlayer(i);
			}
		}


		System.out.println(
				"   >===>          >>       >=>       >=> >=======>           >===>      >=>         >=> >=======> >======>\n"+     
						">>    >=>       >>=>      >> >=>   >>=> >=>               >=>    >=>    >=>       >=>  >=>       >=>    >=>\n"+   
						">=>             >> >=>     >=> >=> > >=> >=>             >=>        >=>   >=>     >=>   >=>       >=>    >=>   \n"+
						">=>            >=>  >=>    >=>  >=>  >=> >=====>         >=>        >=>    >=>   >=>    >=====>   >> >==>      \n"+
						">=>   >===>   >=====>>=>   >=>   >>  >=> >=>             >=>        >=>     >=> >=>     >=>       >=>  >=>     \n"+
						" >=>    >>   >=>      >=>  >=>       >=> >=>               >=>     >=>       >===>      >=>       >=>    >=>   \n"+
				"  >====>    >=>        >=> >=>       >=> >=======>           >===>            >=>       >=======> >=>      >=> \n");


		System.out.println(winningPlayer.getUserName() + " WINS!");

	}


	public static void printOpeningMessages() {


		String title = 
				">======>         >===>      >=>       >=>       >>\n"+       
						">=>    >=>     >=>    >=>   >> >=>   >>=>      >>=>\n"+      
						">=>    >=>   >=>        >=> >=> >=> > >=>     >> >=>\n"+     
						">> >==>      >=>        >=> >=>  >=>  >=>    >=>  >=> \n"+   
						">=>  >=>     >=>        >=> >=>   >>  >=>   >=====>>=>  \n"+ 
						">=>    >=>     >=>     >=>  >=>       >=>  >=>      >=>  \n"+
						">=>      >=>     >===>      >=>       >=> >=>        >=> \n";


		System.out.println(title);

		System.out.println ("Roma - The Card Game:\n");      
	}
	
	
	


	public void initTestGame() {
		running = true;
//		board = new Board();
//		this.setBoard(board);
		Player.initTestPlayer(board);
//		Player.initTestPlayer(board);
		
		
	}
	
	public Board getBoard() {
		return board;
	}
	
	public void setBoard(Board board) {
		this.board = board;
	}
}
