package Game;

import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumMap;
import java.util.List;

import framework.cards.Card;
import framework.interfaces.activators.AesculapinumActivator;

public class RomaGameState implements framework.interfaces.GameState {

	private Board board;
	private Player player;
	
	public RomaGameState() {
		board = new Board();
	}
	
	@Override
	public int getWhoseTurn() {
		
		//return Player.getPlayerNum();
		return Player.getCurrentPlayer().getPlayerNum();
	}

	@Override
	public void setWhoseTurn(int player) {
		Player.setPlayer(player);
		
	}

	@Override
	public List<Card> getDeck() {
		ArrayList<framework.cards.Card> testDeck = new ArrayList<framework.cards.Card>();
		for(Cards.Card c : board.getDeck()) {
			testDeck.add(c.getEnum());
		}
		return testDeck;
		
	}

	@Override
	public void setDeck(List<Card> deck) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<Card> getDiscard() {
		ArrayList<framework.cards.Card> testDeck = new ArrayList<framework.cards.Card>();
		for(Cards.Card c : board.getDiscardPile()) {
			testDeck.add(c.getEnum());
		}
		return testDeck;
	}

	@Override
	public void setDiscard(List<Card> discard) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int getPlayerSestertii(int playerNum) {
		int sestertii = Player.getPlayer(playerNum).getSestertii();
		return sestertii;
	}

	@Override
	public void setPlayerSestertii(int playerNum, int amount) {
		Player.getPlayer(playerNum).setSestertti(amount);
	}

	@Override
	public int getPlayerVictoryPoints(int playerNum) {
		
		return Player.getPlayer(playerNum).getVictoryPoints();
	}

	@Override
	public void setPlayerVictoryPoints(int playerNum, int points) {
		Player.getPlayer(playerNum).setVP(points);
		
	}

	@Override
	public Collection<Card> getPlayerHand(int playerNum) {
		ArrayList<Cards.Card> hand = Player.getPlayer(playerNum).getAllCardsInHand();
		ArrayList<framework.cards.Card> playerHand = new ArrayList<framework.cards.Card>();
		
		for(Cards.Card c : hand) {
			playerHand.add(c.getEnum());
		}
		
		return playerHand;
	}

	@Override
	public void setPlayerHand(int playerNum, Collection<Card> hand) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Card[] getPlayerCardsOnDiscs(int playerNum) {		
		framework.cards.Card[] playerCardsOnDisks = new framework.cards.Card[Config.NUM_DICEDISKS];
		Disk[] diceDisks = board.getDiceDisks();

		for(int i = 0; i<Config.NUM_DICEDISKS; i++) {
			if(diceDisks[i] == null) {
				playerCardsOnDisks[i]=framework.cards.Card.NOT_A_CARD;
			} else {
				playerCardsOnDisks[i]=diceDisks[i].getCard(playerNum).getEnum();
			}
		}
		
		return playerCardsOnDisks;
		
	}

	@Override
	public void setPlayerCardsOnDiscs(int playerNum, Card[] discCards) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int[] getActionDice() {
		return Player.getCurrentPlayer().getDiceValues();
	}

	@Override
	public void setActionDice(int[] dice) {
		Player.getCurrentPlayer().setDiceValues(dice);
		
	}

	@Override
	public int getPoolVictoryPoints() {
		return board.getNumVPLeft();
	}
	
	public Cards.Card convertEnumToCard(framework.cards.Card card) {
		EnumMap<framework.cards.Card, Cards.Card> cardsMap = new EnumMap<framework.cards.Card, Cards.Card>(framework.cards.Card.class);
		cardsMap.put(framework.cards.Card.ARCHITECTUS, Cards.Card.Architectus.class);
	}

}
