package Game;

public class Config {
	
	public static final int NUM_DICEDISKS = 7;
	public static final int BRIBE_DISK = 7;
	
	public static final int NUM_FACES_DIE = 6;
	
	//Player info
	public static final int NUM_INIT_CARDS = 5;	
	public static final int NUM_ACTION_DICE = 3;
	public static final int NUM_PLAYERS = 2;
	public static final int PLAYER1 = 0;
	public static final int PLAYER2 = 1;
	public static final int INIT_SESTERTII = 10;
	
	public static final int NUM_CARDS_INIT_SWAP = 2;
	public static final int NUM_FACE_DOWN_CARDS = 5;
	
	
	//Number of Cards
	public static final int NUM_SICARIUS = 1;
	public static final int NUM_LEGAT = 2;
	public static final int NUM_TRIBUNUS = 2;
	public static final int NUM_ESSEDUM = 2;
   public static final int NUM_ARCHITECTUS = 2;
   public static final int NUM_GLADIATOR = 2;
   public static final int NUM_LEGIONARIUS = 3;
   public static final int NUM_VELITES = 2;
   public static final int NUM_HARUSPEX = 2;
   public static final int NUM_SCAENICUS = 2;
   public static final int NUM_CENTURIO = 2;
   public static final int NUM_CONSUL = 2;
   public static final int NUM_SENATOR = 2;
   public static final int NUM_PRAETORIANUS = 0;
   public static final int NUM_CONSILIARIUS = 2;
   public static final int NUM_MERCATOR = 1;
   public static final int NUM_NERO = 1;
   public static final int NUM_KAT = 2;
   public static final int NUM_GRIMREAPER = 1;
   
   public static final int NUM_AESCULAPINUM = 2;
   public static final int NUM_BASILICA = 2;
   public static final int NUM_MACHINA = 2;
   public static final int NUM_FORUM = 6;
   public static final int NUM_MERCATUS = 2;
   public static final int NUM_ONAGER = 2;
   public static final int NUM_TEMPLUM = 2;
   public static final int NUM_TURRIS = 2;
   public static final int NUM_TELEPHONEBOX = 1;
   
	public static final int DICE_DISK_POS = 0;
	public static final int PLAYER_POS = 1;
	
	public static final int GAME_VICTORY_POINTS = 36;
	public static final int INIT_VP = 10;

	public static final int USED = 1;
	public static final int UNUSED = 0;

	public static final int CHARACTER = 0;
	public static final int BUILDING = 1;
   public static final int NOT_A_CARD = 3;

	public static final int ESSEDUM_DECREMENT = 2;
	public static final int KAT_LIVES = 9;
	
}
