package Game;

import java.io.IOException;
import java.util.ArrayList;

import CardImplimentation.MyBlockedActivator;
import CardImplimentation.MyLegatActivator;
import CardImplimentation.MySicariusActivator;
import Cards.NotACard;

import framework.Rules;
import framework.cards.Card;
import framework.interfaces.GameState;
import framework.interfaces.MoveMaker;
import framework.interfaces.activators.CardActivator;



public class MyMoveMakerImplementation implements MoveMaker {

   private GameState state;
   //	private Roma roma;
   private Board board;



   public MyMoveMakerImplementation(GameState state) {
      this.state = state;
      this.board = Player.getPlayerBoard(); //nothing can justify this shit
   }



   @Override
   public CardActivator chooseCardToActivate(int disc)
         throws UnsupportedOperationException {

      boolean marked = false;
      int currentTestPlayer = state.getWhoseTurn();
      Player.getPlayer(currentTestPlayer).setCurrentDiskPosition(disc);

      for (int i=0; ((i<Player.getPlayer(currentTestPlayer).getDice().size()) && (!marked)); i++) {
         Die currentDie = Player.getPlayer(currentTestPlayer).getDice().get(i);
         if ((!currentDie.isUsed()) && (currentDie.getDieValue() == disc)) {
            currentDie.markUsed();
            marked = true;
         }
      }

      Board board = Player.getPlayerBoard();
      Cards.Card ourCard = board.getDiceDisks()[disc-1].getCard(state.getWhoseTurn());

      CardActivator activator = ourCard.getActivator(disc);

      if (board.getDiceDisks()[disc-1].isBlocked(currentTestPlayer)) {
         marked = false;
         activator = new MyBlockedActivator();
         for (int i=0; ((i<Player.getPlayer(currentTestPlayer).getDice().size()) && (!marked)); i++) {
            Die currentDie = Player.getPlayer(currentTestPlayer).getDice().get(i);
            if ((currentDie.isUsed()) && (currentDie.getDieValue() == disc)) {
               currentDie.resetDie();
               marked = true;
            }
         }
         board.getDiceDisks()[Rules.BRIBE_DISC-1].resetBlocked(currentTestPlayer);
      }
      return activator;
   }


   @Override
   public void activateCardsDisc(int diceToUse, Card chosen)
         throws UnsupportedOperationException {
      boolean marked = false;
      for (int i=0; ((i<Player.getPlayer(state.getWhoseTurn()).getDice().size()) && (!marked)); i++) {
         Die currentDie = Player.getPlayer(state.getWhoseTurn()).getDice().get(i);
         if ((!currentDie.isUsed()) && (currentDie.getDieValue() == diceToUse)) {
            currentDie.markUsed();
            marked = true;
         }
      }
      ArrayList<Cards.Card> drawnCards = board.drawCards(diceToUse);
      Cards.Card myChosenCard = new NotACard(board);
      boolean haveChosen = false;
      for(Cards.Card c : drawnCards) {
         if((!haveChosen) && (c.getEnum() == chosen)) {
            myChosenCard = c;
            haveChosen = true;
         } else {
            board.discard(c);
         }
      }

      Player.getPlayer(state.getWhoseTurn()).receiveCard(myChosenCard);

   }

   @Override
   public void activateMoneyDisc(int diceToUse)
         throws UnsupportedOperationException {
      int currentPlayer = state.getWhoseTurn();
      
      boolean marked = false;
      for (int i=0; ((i<Player.getPlayer(currentPlayer).getDice().size()) && (!marked )); i++) {
         Die currentDie = Player.getPlayer(currentPlayer).getDice().get(i);
         if ((!currentDie.isUsed()) && (currentDie.getDieValue() == diceToUse)) {
            currentDie.markUsed();
            marked = true;
         }
      }
      int currentSes = Player.getPlayer(currentPlayer).getSestertii();
      state.setPlayerSestertii(currentPlayer, (currentSes+diceToUse));
   }

   @Override
   public void endTurn() throws UnsupportedOperationException {
      int currentPlayerId = state.getWhoseTurn();
      for (int i=0; i<Rules.NUM_DICE_DISCS; i++) {
         board.getDiceDisks()[i].resetBlocked(currentPlayerId);
      }
      
      state.setWhoseTurn( (state.getWhoseTurn()+1) % Rules.NUM_PLAYERS );
      currentPlayerId = state.getWhoseTurn();
      int numUnOc = board.getNumUnoccupiedDisks(currentPlayerId);
      int curPlayerVP = state.getPlayerVictoryPoints(currentPlayerId);
      state.setPlayerVictoryPoints(currentPlayerId, (curPlayerVP-numUnOc));

   }

   @Override
   public void placeCard(Card toPlace, int discToPlaceOn)
         throws UnsupportedOperationException {
      
      Cards.Card myCard = testHelper.convertEnumToCard(toPlace, board);
      Player.getPlayer(state.getWhoseTurn()).removeCard(toPlace);
      Player.getPlayer(state.getWhoseTurn()).subtractSestertii(myCard.getCost());
      board.playCard(state.getWhoseTurn(), myCard, discToPlaceOn-1);

   }

   @Override
   public CardActivator activateBribeDisc(int diceToUse)
         throws UnsupportedOperationException {
      boolean marked = false;
      int currentTestPlayer = state.getWhoseTurn();
      Player.getPlayer(currentTestPlayer).setCurrentDiskPosition(Rules.BRIBE_DISC);

      for (int i=0; ((i<Player.getPlayer(currentTestPlayer).getDice().size()) && (!marked)); i++) {
         Die currentDie = Player.getPlayer(currentTestPlayer).getDice().get(i);
         if ((!currentDie.isUsed()) && (currentDie.getDieValue() == diceToUse)) {
            currentDie.markUsed();
            marked = true;
         }
      }

      Board board = Player.getPlayerBoard();
      Cards.Card ourCard = board.getDiceDisks()[Rules.BRIBE_DISC-1].getCard(state.getWhoseTurn());
      int cost = diceToUse;
      int curPlayerSes = state.getPlayerSestertii(currentTestPlayer);
      state.setPlayerSestertii(currentTestPlayer, (curPlayerSes-cost));
      CardActivator activator = ourCard.getActivator(Rules.BRIBE_DISC);

      if (board.getDiceDisks()[Rules.BRIBE_DISC-1].isBlocked(currentTestPlayer)) {
         marked = false;
         activator = new MyBlockedActivator();
         for (int i=0; ((i<Player.getPlayer(currentTestPlayer).getDice().size()) && (!marked)); i++) {
            Die currentDie = Player.getPlayer(currentTestPlayer).getDice().get(i);
            if ((currentDie.isUsed()) && (currentDie.getDieValue() == diceToUse)) {
               currentDie.resetDie();
               marked = true;
            }
         }
         board.getDiceDisks()[Rules.BRIBE_DISC-1].resetBlocked(currentTestPlayer);
      }
      return activator;
   }

}
