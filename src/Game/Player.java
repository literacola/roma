package Game;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

import Cards.*;


public class Player {

	private String userName;
	private int sestertii; //Coins
	private int victoryPoints;
	private ArrayList<Die> actionDice = new ArrayList<Die>();
	private static Board board;
	private int playerNum;
	private static ArrayList<Player> players = new ArrayList<Player>();  //My gut tells me this is wrong, should this get moved somewhere else?
	private ArrayList<Card> cardsInHand = new ArrayList<Card>();
	private Die currentActionDie;
	private int currentDiskPosition;
	private static int curTestPlayer=0;



	public Player (){
		this.userName = "";
		this.sestertii = 0;
		this.victoryPoints = 0;
		this.playerNum = 0;
		this.currentDiskPosition = 0;
		
	}

	public static Player makePlayer(String userName, Board board, int playerNum) {
		Player player = new Player();
		Player.board = board;
		player.setName(userName);
		player.addSestertii(Config.INIT_SESTERTII);
		player.addVictoryPoints(Config.INIT_VP);
		for(int i=0; i<Config.NUM_ACTION_DICE; i++) {
			player.addActionDie(new Die());
		}

		player.setPlayerNum(playerNum);
		player.receiveCards(board.drawCards(Config.NUM_INIT_CARDS));
		
		return player;
	}
	
	public static Player makeTestPlayer(String userName, Board board, int playerNum) {
		Player player = new Player();
		player.setName(userName);
		Player.board = board;
		player.setPlayerNum(playerNum);
		for(int i=0; i<Config.NUM_ACTION_DICE; i++) {
			player.addActionDie(new Die());
		}
		return player;
	}
	

	public static Player getPlayer(int playerID) {
		assert(playerID < Config.NUM_PLAYERS);
		return(players.get(playerID));
	}


	public void displayActionDiceValues() {
		int[] values = new int[Config.NUM_ACTION_DICE];
		values=this.getDiceValues();
		System.out.print(this.getUserName() + " rolled: ");
		for(int die : values) {
			System.out.print(die + ", ");
		}
		System.out.println("");

	}


	public void receiveCards(ArrayList<Card> cards) {
		this.cardsInHand.addAll(cards);
	}

	public void receiveCard(Card card) {
		this.cardsInHand.add(card);
	}

	public ArrayList<Card> getAllCardsInHand() {
		return(this.cardsInHand);
	}

	public Card getCard(int index) {
		return(this.cardsInHand.get(index));
	}

	public Card playCard(int index) {
		Card tmp = this.getCard(index);
		this.removeCard(tmp);
		return(tmp);
	}

	public void removeCard(int cardIndex) {
		cardsInHand.remove(cardIndex);
	}


	public void removeCard(Card card) {
		cardsInHand.remove(card);
	}


	public void displayCards() {
		System.out.println("Player " + this.getUserName() + " has the following cards in hand:");
		for(int i=0; i<cardsInHand.size(); i++) {
			System.out.println("["+i+"] " + this.cardsInHand.get(i).getName());
		}
	}

	public void rollActionDice(){
		for(int i=0; i< Config.NUM_ACTION_DICE; i++) {
			actionDice.get(i).rollDie();
		}

	}


	public ArrayList<Die> getDice() {
		return actionDice;
	}

	public ArrayList<Die> getUnusedDice() {
		ArrayList<Die> unusedDice = new ArrayList<Die>();

		for(Die d: actionDice) {
		   if (!d.isUsed()) {
		      
				unusedDice.add(d);
			}
		}

		return unusedDice;
	}
	
	public int[] getUnusedDiceIndex() {
		ArrayList<Die> unusedDice = new ArrayList<Die>();
		int[] getUnusedDiceIndex = new int[this.getUnusedDice().size()+1];
		
		int i = 0;
		int j = 0;
		
		for(Die d: actionDice) {
			
			if (!d.isUsed()) {
				unusedDice.add(d);
				getUnusedDiceIndex[j] = i;
				j++;
			}
			i++;
		}

		return getUnusedDiceIndex;
	}

	public int[] getUnusedDiceArray() {
      ArrayList<Die> unusedDice = getUnusedDice();
      int[] unusedDiceArray = new int[this.getUnusedDice().size()];
      for (int i=0; i<unusedDice.size(); i++) {
         unusedDiceArray[i] = unusedDice.get(i).getDieValue();
      }

      return unusedDiceArray;
   }
	
	public int[] getDiceValues() {

		int[] diceValues = new int[Config.NUM_ACTION_DICE];

		for(int i=0; i< Config.NUM_ACTION_DICE; i++) {
			diceValues[i] = actionDice.get(i).getDieValue();
		}

		return diceValues;
	}

	public void barfPlayerInfo(){
		System.out.println("Player " + this.getUserName());
		System.out.println("Sestertii: " + this.getSestertii());
		System.out.println("Victory Points: " + this.getVictoryPoints());
		System.out.print("ActionDice Values: ");
		this.displayActionDiceValues();
		this.displayCards();

	}

	public static void initializePlayers(Board board, UserInput userIn) throws IOException {
		String playerName;
		Player player;
		System.out.println("Welcome to Roma, the youngest player must enter their name first: ");
		for(int i=0; i<Config.NUM_PLAYERS; i++) {
			playerName = userIn.getUserInput("Enter name for player " + (i+1) + ": ");
			player = makePlayer(playerName, board, i);
			players.add(player);
		}
	}
	
	public static void initTestPlayer(Board board) {
		Player player1 = makeTestPlayer("Player1", board, 0);
		Player player2 = makeTestPlayer("Player2", board, 1);
		players.add(player1);
		players.add(player2);
	}


	public static Player getCurrentPlayer() {
		int playerNum = Roma.getTurn() % Config.NUM_PLAYERS ;
		return(players.get(playerNum));

	}

	public static ArrayList<Player> getOpposingPlayers(Player currentPlayer) {
		ArrayList<Player> opposingPlayers = new ArrayList<Player>(); 

		for(Player p : players) {
			if(!p.equals(currentPlayer)){
				opposingPlayers.add(p);
			}
		}

		return(opposingPlayers);
	}


	public static int[] getAllPlayerVP() {
		int[] allPlayerVP = new int[Config.NUM_PLAYERS];

		for(int i=0; i<Config.NUM_PLAYERS; i++) {
			allPlayerVP[i] = players.get(i).getVictoryPoints();
		}

		return(allPlayerVP);

	}


	//standard getters and setters

	public int getVictoryPoints(){
		return this.victoryPoints;
	}


	public int getSestertii(){
		return this.sestertii;
	}

	public void addSestertii(int additionalSestertii){
		this.sestertii = this.sestertii+additionalSestertii;
	}

	public void subtractSestertii(int subtractedSestertii){
		this.sestertii = this.sestertii-subtractedSestertii;
	}

	public void addVictoryPoints(int additionalVictoryPoints){
		this.victoryPoints = this.victoryPoints+additionalVictoryPoints;
		board.subtractVP(additionalVictoryPoints);
	}

	public void subtractVictoryPoints(int subtractedVictoryPoints){
		if(subtractedVictoryPoints > this.victoryPoints) {
			this.victoryPoints = 0;
		} else {
			this.victoryPoints = this.victoryPoints-subtractedVictoryPoints;
		}
		board.addVP(subtractedVictoryPoints);
	}

	public String getUserName() {
		return userName;
	}


	public int getPlayerNum() {
		return playerNum;
	}


	public void setPlayerNum(int playerNum) {
		this.playerNum = playerNum;
	}


	public static void setPlayer(int player) {
		players.get(player);

	}


	public void setSestertti(int amount) {
		this.sestertii = amount;

	}


	public void setVP(int points) {
		int opponent = 1 ^ this.playerNum;
		int opponentVP = getPlayer(opponent).getVictoryPoints();
		this.victoryPoints = points;
		board.setBoardVP(Config.GAME_VICTORY_POINTS - (points + opponentVP) );
	}


	public void setDiceValues(int[] dice) {
		for(int i=0; i< dice.length; i++) {
		   actionDice.get(i).resetDie();
			actionDice.get(i).setDieValue(dice[i]);
		}
		
	}


	public static int getOpponent(){
		int opponent;
		if (Player.getCurrentPlayer().getPlayerNum() == 0) {
			opponent = 1;
		} else {
			opponent = 0;
		}
		return opponent;
	}


	public void incrementDie(int diceChoice) {
		actionDice.get(diceChoice).incDieValue();
	}

	public void decrementDie(int diceChoice) {
		actionDice.get(diceChoice).decDieValue();
	}


	public Die getCurrentActionDie() {
		return currentActionDie;
	}


	public void setCurrentActionDie(Die currentActionDie) {
		this.currentActionDie = currentActionDie;
	}


	public void setCurrentDiskPosition(int dieValue) {
		this.currentDiskPosition = dieValue -1;		
	}
	
	public int getCurrentDiskPosition() {
		return this.currentDiskPosition;
	}
	
	public static void setTestPlayer(int playerID) {
		curTestPlayer = playerID;
	}
	
	public static int getTestPlayer() {
		return curTestPlayer;
	}

	public static int getOpposingTestPlayer() {
		int opponent  = 1 ^ curTestPlayer;
		return opponent;
   }
	
	public void setName(String name) {
		this.userName = name;
	}
	
	public void addActionDie(Die die) {
		actionDice.add(die);
	}
	
	public  static Board getPlayerBoard() {
		return board;
	}
	
	public void barfBoard() {
		this.board.printBoard(false);
	}

	public void receiveNewCards(ArrayList<Cards.Card> playerHand) {
		this.cardsInHand.clear();
		this.cardsInHand.addAll(playerHand);		
	}

	public void setNewVP(int points) {
		this.victoryPoints = points;
		board.setBoardVP(points);		
	}
	
	public void removeCard(framework.cards.Card card) {
		Card removeMe = null;
		
		for (Card c : cardsInHand) {
			if(c.getEnum() == card) {
				removeMe=c;
			}
		}		
		cardsInHand.remove(removeMe);
	}
	

}