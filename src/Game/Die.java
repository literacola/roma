package Game;
import java.util.Random;



public class Die {
	final int NUM_DICE_FACES = 6;
	private int dieValue;
	private boolean used = false;

	Random rand = new Random();

	public void rollDie(){
		this.dieValue = Math.abs(((rand.nextInt()) % NUM_DICE_FACES))+1;
		this.resetDie();
	}

	public int getDieValue(){
		return(this.dieValue);
	}

	public void setDieValue(int i) {
		this.dieValue = i;

	}

	public void incDieValue() {
		if(dieValue+1 <= Config.NUM_FACES_DIE){
			this.dieValue++;
		}
	}

	public void decDieValue() {
		if(dieValue-1 >= 0){
			this.dieValue--;
		}
	}

	public void resetDie() {
		this.used = false;
	}

	public void markUsed() {
		this.used = true;
	}

	public boolean isUsed() {
		return this.used;
	}
	
	public String toString(){
		return(String.valueOf(this.dieValue));
	}

}
